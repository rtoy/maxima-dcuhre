(in-package "MAXIMA")


(defmfun $toms698_dcuhre (fcns vars a b &key (key 0)
			       (minpts 0) (maxpts nil maxpts-p) (epsabs 1e-10) (epsrel 0))
  ;; Some error checking
  ;; Key must be given and must be either 0, 1, 2, 3, or 4.
  (unless (and (integerp key) (<= 0 key 4))
    (merror "~M: key must be an integer between 0 and 4 inclusive, not ~M"
	    %%pretty-fname key))
  (unless (and (integerp minpts) (>= minpts 0))
    (merror "~M: minpts must a non-negative integer, not ~M"
	    %%pretty-fname minpts))
  (unless (and (realp epsabs) (>= epsabs 0))
    (merror "~M: epsabs must be a non-negative number, not ~M"
	    %%pretty-fname epsabs))
  (unless (and (realp epsrel) (>= epsrel 0))
    (merror "~M: epsrel must be a non-negative number, not ~M"
	    %%pretty-fname epsrel))

  (unless (= ($length a) ($length vars))
    (merror "~M: length of a must match length of vars but got ~M"
	    %%pretty-fname ($length vars)))

  (unless (= ($length b) ($length vars))
    (merror "~M: length of b must match length of vars but got ~M"
	    %%pretty-fname ($length vars)))
  
  (let* ((epsabs ($float epsabs))
	 (epsrel ($float epsrel))
	 (ndim ($length vars))
	 (numfun ($length fcns))
	 (fv (coerce-float-fun fcns vars))
	 (a (make-array ndim
			:element-type 'double-float
			:initial-contents (mapcar #'(lambda (x)
						      ($float x))
						  (cdr a))))
	 (b (make-array ndim
			:element-type 'double-float
			:initial-contents (mapcar #'(lambda (x)
						      ($float x))
						  (cdr b))))
	 (num
	   ;; This is from dchhre.f, lines 163-191, not dcuhre.f.  If
	   ;; we use the formulas from dcuhre.f, we get failures that
	   ;; num or maxpts is too small.
	   (let ((keyf (if (zerop key)
			   (cond ((= ndim 2)
				  1)
				 ((= ndim 3)
				  2)
				 (t
				  3))
			   key)))
	     (cond ((= keyf 1)
		    65)
		   ((= keyf 2)
		    127)
		   ((= keyf 3)
		    (+ 1
		       (* 4 2 ndim)
		       (* 2 ndim (1- ndim))
		       (* 4 ndim (1- ndim))
		       (/ (* 4 ndim (1- ndim) (- ndim 2))
			  3)
		       (expt 2 ndim)))
		   ((= keyf 4)
		    (+ 1
		       (* 3 2 ndim)
		       (* 2 ndim (1- ndim))
		       (expt 2 ndim))))))
	 (min-maxpts (* 3 num))
	 (mdiv 1)
	 (result (make-array numfun :element-type 'double-float))
	 (abserr (make-array numfun :element-type 'double-float))
	 (restar 0))

    (when (and maxpts-p (integerp maxpts) (< maxpts min-maxpts) (plusp maxpts))
      (merror "~M:  maxpts must be an positive integer of at least ~M but got ~M"
	      %%pretty-fname min-maxpts maxpts))

    (let* ((maxpts (if maxpts-p
		       maxpts
		       min-maxpts))
	   (maxsub (+ 1 (floor (/ (- maxpts num) (* 2 num)))))
	   (nw (+ 1
		  (* 17 numfun mdiv)
		  (* maxsub
		     (+ (* 2 ndim)
			(* 2 numfun)
			2))))
	   (work (make-array nw :element-type 'double-float)))

      (multiple-value-bind (ignore-ndim
			    ignore-numfun ignore-a ignore-b ignore-minpts ignore-maxpts
			    ignore-funsub ignore-epsabs ignore-epsrel ignore-key ignore-nw
			    ignore-restar ignore-result ignore-abserr
			    val-neval val-ifail 
			    ignore-work)
	  (toms698::dcuhre ndim
			   numfun
			   a b minpts maxpts
			   #'(lambda (ndim x numfun funvls)
			       (declare (ignorable numfun))
			       (let ((f (apply 'funcall fv (coerce (subseq x 0 ndim) 'list))))
				 (loop for k from 0
				       for v in (cdr f)
				       do
					  (setf (aref funvls k) (coerce v 'double-float)))
				 (values nil nil nil nil)))
			   epsabs epsrel
			   key nw restar result abserr 0 0 work)
	(declare (ignore ignore-ndim ignore-numfun ignore-a ignore-b ignore-minpts ignore-maxpts
			 ignore-funsub ignore-epsabs ignore-epsrel ignore-key ignore-nw
			 ignore-restar ignore-result ignore-abserr ignore-work))
	(list '(mlist)
	      val-ifail
	      (list* '(mlist) (coerce result 'list))
	      (list* '(mlist) (coerce abserr 'list))
	      val-neval)))))