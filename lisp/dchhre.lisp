;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':simple-array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun dchhre
       (maxdim ndim numfun mdiv a b minpts maxpts epsabs epsrel key nw restar
        num maxsub minsub keyf ifail wtleng)
  (declare (type (double-float) epsrel epsabs)
           (type (simple-array double-float (*)) b a)
           (type (f2cl-lib:integer4) wtleng ifail keyf minsub maxsub num restar
                                     nw key maxpts minpts mdiv numfun ndim
                                     maxdim))
  (prog ((limit 0) (j 0))
    (declare (type (f2cl-lib:integer4) j limit))
    (setf ifail 0)
    (cond
      ((or (< key 0) (> key 4))
       (setf ifail 2)
       (go label999)))
    (cond
      ((or (< ndim 2) (> ndim maxdim))
       (setf ifail 3)
       (go label999)))
    (cond
      ((and (= key 1) (/= ndim 2))
       (setf ifail 4)
       (go label999)))
    (cond
      ((and (= key 2) (/= ndim 3))
       (setf ifail 5)
       (go label999)))
    (cond
      ((= key 0)
       (cond
         ((= ndim 2)
          (setf keyf 1))
         ((= ndim 3)
          (setf keyf 2))
         (t
          (setf keyf 3))))
      (t
       (setf keyf key)))
    (cond
      ((= keyf 1)
       (setf num 65)
       (setf wtleng 14))
      ((= keyf 2)
       (setf num 127)
       (setf wtleng 13))
      ((= keyf 3)
       (setf num
               (+ 1
                  (f2cl-lib:int-mul 4 2 ndim)
                  (f2cl-lib:int-mul 2 ndim (f2cl-lib:int-sub ndim 1))
                  (f2cl-lib:int-mul 4 ndim (f2cl-lib:int-sub ndim 1))
                  (the f2cl-lib:integer4
                       (truncate (* 4 ndim (- ndim 1) (- ndim 2)) 3))
                  (expt 2 ndim)))
       (setf wtleng 9)
       (if (= ndim 2) (setf wtleng 8)))
      ((= keyf 4)
       (setf num
               (f2cl-lib:int-add 1
                                 (f2cl-lib:int-mul 3 2 ndim)
                                 (f2cl-lib:int-mul 2
                                                   ndim
                                                   (f2cl-lib:int-sub ndim 1))
                                 (expt 2 ndim)))
       (setf wtleng 6)))
    (setf maxsub
            (+ (the f2cl-lib:integer4 (truncate (- maxpts num) (* 2 num))) 1))
    (setf minsub
            (+ (the f2cl-lib:integer4 (truncate (- minpts num) (* 2 num))) 1))
    (cond
      ((/=
        (mod (f2cl-lib:int-add minpts (f2cl-lib:int-sub num))
             (f2cl-lib:int-mul 2 num))
        0)
       (setf minsub (f2cl-lib:int-add minsub 1))))
    (setf minsub
            (max (the f2cl-lib:integer4 2) (the f2cl-lib:integer4 minsub)))
    (cond
      ((< numfun 1)
       (setf ifail 6)
       (go label999)))
    (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                  ((> j ndim) nil)
      (tagbody
        (cond
          ((=
            (+ (f2cl-lib:fref a (j) ((1 ndim)))
               (- (f2cl-lib:fref b (j) ((1 ndim)))))
            0)
           (setf ifail 7)
           (go label999)))
       label10))
    (cond
      ((< maxpts (f2cl-lib:int-mul 3 num))
       (setf ifail 8)
       (go label999)))
    (cond
      ((< maxpts minpts)
       (setf ifail 9)
       (go label999)))
    (cond
      ((and (< epsabs 0) (< epsrel 0))
       (setf ifail 10)
       (go label999)))
    (setf limit
            (f2cl-lib:int-add
             (f2cl-lib:int-mul maxsub
                               (f2cl-lib:int-add (f2cl-lib:int-mul 2 ndim)
                                                 (f2cl-lib:int-mul 2 numfun)
                                                 2))
             (f2cl-lib:int-mul 17 mdiv numfun)
             1))
    (cond
      ((< nw limit)
       (setf ifail 11)
       (go label999)))
    (cond
      ((and (/= restar 0) (/= restar 1))
       (setf ifail 12)
       (go label999)))
   label999
    (go end_label)
   end_label
    (return
     (values nil
             nil
             nil
             nil
             nil
             nil
             nil
             nil
             nil
             nil
             nil
             nil
             nil
             num
             maxsub
             minsub
             keyf
             ifail
             wtleng))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::dchhre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (simple-array double-float (*))
                        (simple-array double-float (*))
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (double-float) (double-float)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4))
           :return-values '(nil nil nil nil nil nil nil nil nil nil nil nil nil
                            fortran-to-lisp::num fortran-to-lisp::maxsub
                            fortran-to-lisp::minsub fortran-to-lisp::keyf
                            fortran-to-lisp::ifail fortran-to-lisp::wtleng)
           :calls 'nil)))

