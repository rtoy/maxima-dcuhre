;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':simple-array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(let* ((nw 5000))
  (declare (type (f2cl-lib:integer4 5000 5000) nw) (ignorable nw))
  (defun dtest2 ()
    (prog ((a (make-array 10 :element-type 'double-float))
           (b (make-array 10 :element-type 'double-float))
           (wrkstr (make-array nw :element-type 'double-float)) (abserr 0.0d0)
           (n 0))
      (declare (type (simple-array double-float (10)) a b)
               (type (simple-array double-float (*)) wrkstr)
               (type (double-float) abserr)
               (type (f2cl-lib:integer4) n))
      (f2cl-lib:fdo (n 1 (f2cl-lib:int-add n 1))
                    ((> n 10) nil)
        (tagbody
          (setf (f2cl-lib:fref a (n) ((1 10)))
                  (coerce (the f2cl-lib:integer4 0) 'double-float))
          (setf (f2cl-lib:fref b (n) ((1 10)))
                  (coerce (the f2cl-lib:integer4 1) 'double-float))
         label10))
      (setf abserr (coerce 1.0e-10 'double-float))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 2 a b 195 16 #'ftestp abserr 1 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 3 a b 381 16 #'ftestp abserr 2 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 4 a b 459 16 #'ftestp abserr 3 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 5 a b 309 16 #'ftestp abserr 4 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 6 a b 3000 1 #'ftesto abserr 4 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 6 a b 6000 1 #'ftesto abserr 4 nw 1 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 6 a b 12000 1 #'ftesto abserr 4 nw 1 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 2 a b 500 1 #'ftestx abserr 1 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 3 a b 1500 1 #'ftestx abserr 2 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 5 a b 5000 1 #'ftestx abserr 3 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 7 a b 10000 1 #'ftestx abserr 4 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9
             var-10)
          (atest 10 a b 20000 1 #'ftestx abserr 4 nw 0 wrkstr)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-7 var-8 var-9
                         var-10))
        (setf abserr var-6))
     end_label
      (return nil))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::dtest2
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo :arg-types 'nil
                                            :return-values 'nil
                                            :calls '(fortran-to-lisp::atest))))

;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':simple-array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(let ((neval 0)
      (absest (make-array 20 :element-type 'double-float))
      (finest (make-array 20 :element-type 'double-float)))
  (declare (type (f2cl-lib:integer4) neval)
           (type (simple-array double-float (20)) absest finest))
  (defun atest (ndim a b maxcls nfun tstsub abserr key lenwrk irest wrkstr)
    (declare (type (double-float) abserr)
             (type (simple-array double-float (*)) wrkstr b a)
             (type (f2cl-lib:integer4) irest lenwrk key nfun maxcls ndim))
    (prog ((n 0) (ifail 0) (rel 0.0d0))
      (declare (type (double-float) rel) (type (f2cl-lib:integer4) ifail n))
      (setf rel (coerce (the f2cl-lib:integer4 0) 'double-float))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9 var-10
             var-11 var-12 var-13 var-14 var-15 var-16)
          (dcuhre ndim nfun a b 0 maxcls tstsub abserr rel key lenwrk irest
           finest absest neval ifail wrkstr)
        (declare (ignore var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9 var-10
                         var-11 var-12 var-13 var-16))
        (setf ndim var-0)
        (setf nfun var-1)
        (setf neval var-14)
        (setf ifail var-15))
      (f2cl-lib:fformat t
                        ("~%" "~5@T" "DCUHRE TEST WITH NDIM = " 1 (("~3D"))
                         ", KEY = " 1 (("~2D")) "~%")
                        ndim
                        key)
      (f2cl-lib:fformat t
                        ("~5@T" "SUBROUTINE CALLS = " 1 (("~6D")) ", IFAIL = "
                         1 (("~2D")) "~%")
                        neval
                        ifail)
      (f2cl-lib:fformat t ("~7@T" "N   ABSOLUTE ERROR    INTEGRAL" "~%"))
      (f2cl-lib:fdo (n 1 (f2cl-lib:int-add n 1))
                    ((> n nfun) nil)
        (tagbody
          (f2cl-lib:fformat t
                            ("~6@T" 1 (("~2D")) 1 (("~14,2,2,0,'*,,'EE")) 1
                             (("~21,16,0,'*,F")) "~%")
                            n
                            (abs (- (f2cl-lib:fref finest (n) ((1 20))) 1))
                            (f2cl-lib:fref finest (n) ((1 20))))
         label10))
     end_label
      (return (values ndim nil nil nil nfun nil abserr nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::atest fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4)
                        (simple-array double-float (*))
                        (simple-array double-float (*))
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        t (double-float) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (simple-array double-float (*)))
           :return-values '(fortran-to-lisp::ndim nil nil nil
                            fortran-to-lisp::nfun nil fortran-to-lisp::abserr
                            nil nil nil nil)
           :calls '(fortran-to-lisp::dcuhre))))

