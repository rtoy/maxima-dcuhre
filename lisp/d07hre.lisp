;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun d07hre (ndim wtleng w g errcof rulpts)
  (declare (type (array double-float (*)) errcof)
           (type (array double-float (*)) rulpts g w)
           (type (f2cl-lib:integer4) wtleng ndim))
  (f2cl-lib:with-multi-array-data
      ((w double-float w-%data% w-%offset%)
       (g double-float g-%data% g-%offset%)
       (rulpts double-float rulpts-%data% rulpts-%offset%)
       (errcof double-float errcof-%data% errcof-%offset%))
    (prog ((i 0) (j 0) (ratio 0.0d0) (lam0 0.0d0) (lam1 0.0d0) (lam2 0.0d0)
           (lamp 0.0d0) (twondm 0.0d0))
      (declare (type (double-float) twondm lamp lam2 lam1 lam0 ratio)
               (type (f2cl-lib:integer4) j i))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j wtleng) nil)
        (tagbody
          (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                        ((> i ndim) nil)
            (tagbody
              (setf (f2cl-lib:fref g-%data%
                                   (i j)
                                   ((1 ndim) (1 wtleng))
                                   g-%offset%)
                      (coerce (the f2cl-lib:integer4 0) 'double-float))
             label10))
          (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                        ((> i 5) nil)
            (tagbody
              (setf (f2cl-lib:fref w-%data%
                                   (i j)
                                   ((1 5) (1 wtleng))
                                   w-%offset%)
                      (coerce (the f2cl-lib:integer4 0) 'double-float))
             label20))
          (setf (f2cl-lib:fref rulpts-%data% (j) ((1 wtleng)) rulpts-%offset%)
                  (coerce (the f2cl-lib:integer4 (f2cl-lib:int-mul 2 ndim))
                          'double-float))
         label30))
      (setf twondm
              (coerce (the f2cl-lib:integer4 (expt 2 ndim)) 'double-float))
      (setf (f2cl-lib:fref rulpts-%data% (wtleng) ((1 wtleng)) rulpts-%offset%)
              twondm)
      (setf (f2cl-lib:fref rulpts-%data%
                           ((f2cl-lib:int-sub wtleng 1))
                           ((1 wtleng))
                           rulpts-%offset%)
              (coerce
               (the f2cl-lib:integer4
                    (f2cl-lib:int-mul 2 ndim (f2cl-lib:int-sub ndim 1)))
               'double-float))
      (setf (f2cl-lib:fref rulpts-%data% (1) ((1 wtleng)) rulpts-%offset%)
              (coerce (the f2cl-lib:integer4 1) 'double-float))
      (setf lam0 (coerce 0.4707 'double-float))
      (setf lamp (coerce 0.5625 'double-float))
      (setf lam1 (/ 4 (+ 15 (/ -5 lam0))))
      (setf ratio (/ (+ 1 (/ (- lam1) lam0)) 27))
      (setf lam2
              (/ (- 5 (* 7 lam1) (* 35 ratio))
                 (+ 7 (/ (* -35 lam1) 3) (/ (* -35 ratio) lam0))))
      (setf (f2cl-lib:fref w-%data% (1 6) ((1 5) (1 wtleng)) w-%offset%)
              (/ (/ 1 (expt (* 3 lam0) 3)) twondm))
      (setf (f2cl-lib:fref w-%data% (1 5) ((1 5) (1 wtleng)) w-%offset%)
              (/ (+ 1 (/ (* -5 lam0) 3)) (* 60 (- lam1 lam0) (expt lam1 2))))
      (setf (f2cl-lib:fref w-%data% (1 3) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (+ 1
                   (/ (* -5 lam2) 3)
                   (* -5
                      twondm
                      (f2cl-lib:fref w-%data%
                                     (1 6)
                                     ((1 5) (1 wtleng))
                                     w-%offset%)
                      lam0
                      (- lam0 lam2)))
                (* 10 lam1 (- lam1 lam2)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (f2cl-lib:fref w-%data%
                                 (1 5)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (1 2) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (/ (* -5 lam1) 3)
                  (* -5
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (1 6)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     lam0
                     (- lam0 lam1)))
               (* 10 lam2 (- lam2 lam1))))
      (setf (f2cl-lib:fref w-%data% (2 6) ((1 5) (1 wtleng)) w-%offset%)
              (/ (/ 1 (* 36 (expt lam0 3))) twondm))
      (setf (f2cl-lib:fref w-%data% (2 5) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (* -9
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (2 6)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     (expt lam0 2)))
               (* 36 (expt lam1 2))))
      (setf (f2cl-lib:fref w-%data% (2 3) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (+ 1
                   (/ (* -5 lam2) 3)
                   (* -5
                      twondm
                      (f2cl-lib:fref w-%data%
                                     (2 6)
                                     ((1 5) (1 wtleng))
                                     w-%offset%)
                      lam0
                      (- lam0 lam2)))
                (* 10 lam1 (- lam1 lam2)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (f2cl-lib:fref w-%data%
                                 (2 5)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (2 2) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (/ (* -5 lam1) 3)
                  (* -5
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (2 6)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     lam0
                     (- lam0 lam1)))
               (* 10 lam2 (- lam2 lam1))))
      (setf (f2cl-lib:fref w-%data% (3 6) ((1 5) (1 wtleng)) w-%offset%)
              (/ (/ 5 (* 108 (expt lam0 3))) twondm))
      (setf (f2cl-lib:fref w-%data% (3 5) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (* -9
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (3 6)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     (expt lam0 2)))
               (* 36 (expt lam1 2))))
      (setf (f2cl-lib:fref w-%data% (3 3) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (+ 1
                   (/ (* -5 lamp) 3)
                   (* -5
                      twondm
                      (f2cl-lib:fref w-%data%
                                     (3 6)
                                     ((1 5) (1 wtleng))
                                     w-%offset%)
                      lam0
                      (- lam0 lamp)))
                (* 10 lam1 (- lam1 lamp)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (f2cl-lib:fref w-%data%
                                 (3 5)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (3 4) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (/ (* -5 lam1) 3)
                  (* -5
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (3 6)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     lam0
                     (- lam0 lam1)))
               (* 10 lamp (- lamp lam1))))
      (setf (f2cl-lib:fref w-%data% (4 6) ((1 5) (1 wtleng)) w-%offset%)
              (/ (/ 1 (* 54 (expt lam0 3))) twondm))
      (setf (f2cl-lib:fref w-%data% (4 5) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (* -18
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (4 6)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     (expt lam0 2)))
               (* 72 (expt lam1 2))))
      (setf (f2cl-lib:fref w-%data% (4 3) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (+ 1
                   (/ (* -10 lam2) 3)
                   (* -10
                      twondm
                      (f2cl-lib:fref w-%data%
                                     (4 6)
                                     ((1 5) (1 wtleng))
                                     w-%offset%)
                      lam0
                      (- lam0 lam2)))
                (* 20 lam1 (- lam1 lam2)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (f2cl-lib:fref w-%data%
                                 (4 5)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (4 2) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (/ (* -10 lam1) 3)
                  (* -10
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (4 6)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     lam0
                     (- lam0 lam1)))
               (* 20 lam2 (- lam2 lam1))))
      (setf lam0 (f2cl-lib:fsqrt lam0))
      (setf lam1 (f2cl-lib:fsqrt lam1))
      (setf lam2 (f2cl-lib:fsqrt lam2))
      (setf lamp (f2cl-lib:fsqrt lamp))
      (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                    ((> i ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref g-%data%
                               (i wtleng)
                               ((1 ndim) (1 wtleng))
                               g-%offset%)
                  lam0)
         label40))
      (setf (f2cl-lib:fref g-%data%
                           (1 (f2cl-lib:int-sub wtleng 1))
                           ((1 ndim) (1 wtleng))
                           g-%offset%)
              lam1)
      (setf (f2cl-lib:fref g-%data%
                           (2 (f2cl-lib:int-sub wtleng 1))
                           ((1 ndim) (1 wtleng))
                           g-%offset%)
              lam1)
      (setf (f2cl-lib:fref g-%data%
                           (1 (f2cl-lib:int-sub wtleng 4))
                           ((1 ndim) (1 wtleng))
                           g-%offset%)
              lam2)
      (setf (f2cl-lib:fref g-%data%
                           (1 (f2cl-lib:int-sub wtleng 3))
                           ((1 ndim) (1 wtleng))
                           g-%offset%)
              lam1)
      (setf (f2cl-lib:fref g-%data%
                           (1 (f2cl-lib:int-sub wtleng 2))
                           ((1 ndim) (1 wtleng))
                           g-%offset%)
              lamp)
      (setf (f2cl-lib:fref w-%data% (1 1) ((1 5) (1 wtleng)) w-%offset%)
              twondm)
      (f2cl-lib:fdo (j 2 (f2cl-lib:int-add j 1))
                    ((> j 5) nil)
        (tagbody
          (f2cl-lib:fdo (i 2 (f2cl-lib:int-add i 1))
                        ((> i wtleng) nil)
            (tagbody
              (setf (f2cl-lib:fref w-%data%
                                   (j i)
                                   ((1 5) (1 wtleng))
                                   w-%offset%)
                      (-
                       (f2cl-lib:fref w-%data%
                                      (j i)
                                      ((1 5) (1 wtleng))
                                      w-%offset%)
                       (f2cl-lib:fref w-%data%
                                      (1 i)
                                      ((1 5) (1 wtleng))
                                      w-%offset%)))
              (setf (f2cl-lib:fref w-%data%
                                   (j 1)
                                   ((1 5) (1 wtleng))
                                   w-%offset%)
                      (-
                       (f2cl-lib:fref w-%data%
                                      (j 1)
                                      ((1 5) (1 wtleng))
                                      w-%offset%)
                       (*
                        (f2cl-lib:fref rulpts-%data%
                                       (i)
                                       ((1 wtleng))
                                       rulpts-%offset%)
                        (f2cl-lib:fref w-%data%
                                       (j i)
                                       ((1 5) (1 wtleng))
                                       w-%offset%))))
             label50))
         label70))
      (f2cl-lib:fdo (i 2 (f2cl-lib:int-add i 1))
                    ((> i wtleng) nil)
        (tagbody
          (setf (f2cl-lib:fref w-%data% (1 i) ((1 5) (1 wtleng)) w-%offset%)
                  (* twondm
                     (f2cl-lib:fref w-%data%
                                    (1 i)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)))
          (setf (f2cl-lib:fref w-%data% (1 1) ((1 5) (1 wtleng)) w-%offset%)
                  (-
                   (f2cl-lib:fref w-%data% (1 1) ((1 5) (1 wtleng)) w-%offset%)
                   (*
                    (f2cl-lib:fref rulpts-%data%
                                   (i)
                                   ((1 wtleng))
                                   rulpts-%offset%)
                    (f2cl-lib:fref w-%data%
                                   (1 i)
                                   ((1 5) (1 wtleng))
                                   w-%offset%))))
         label80))
      (setf (f2cl-lib:fref errcof-%data% (1) ((1 6)) errcof-%offset%)
              (coerce (the f2cl-lib:integer4 5) 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (2) ((1 6)) errcof-%offset%)
              (coerce (the f2cl-lib:integer4 5) 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (3) ((1 6)) errcof-%offset%)
              (coerce (the f2cl-lib:integer4 1) 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (4) ((1 6)) errcof-%offset%)
              (coerce (the f2cl-lib:integer4 5) 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (5) ((1 6)) errcof-%offset%)
              (coerce 0.5 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (6) ((1 6)) errcof-%offset%)
              (coerce 0.25 'double-float))
     end_label
      (return (values nil nil nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::d07hre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*)))
           :return-values '(nil nil nil nil nil nil)
           :calls 'nil)))

