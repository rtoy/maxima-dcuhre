;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun ftest (ndim z nfun f)
  (declare (type (array double-float (*)) f z)
           (type (f2cl-lib:integer4) nfun ndim))
  (f2cl-lib:with-multi-array-data
      ((z double-float z-%data% z-%offset%)
       (f double-float f-%data% f-%offset%))
    (prog ((sum 0.0d0) (n 0))
      (declare (type (f2cl-lib:integer4) n) (type (double-float) sum))
      (setf sum (coerce (the f2cl-lib:integer4 0) 'double-float))
      (f2cl-lib:fdo (n 1 (f2cl-lib:int-add n 1))
                    ((> n ndim) nil)
        (tagbody
          (setf sum
                  (+ sum
                     (* n
                        (expt
                         (f2cl-lib:fref z-%data% (n) ((1 ndim)) z-%offset%)
                         2))))
         label10))
      (setf (f2cl-lib:fref f-%data% (1) ((1 nfun)) f-%offset%)
              (exp (/ (- sum) 2)))
      (f2cl-lib:fdo (n 1 (f2cl-lib:int-add n 1))
                    ((> n ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref f-%data%
                               ((f2cl-lib:int-add n 1))
                               ((1 nfun))
                               f-%offset%)
                  (* (f2cl-lib:fref z-%data% (n) ((1 ndim)) z-%offset%)
                     (f2cl-lib:fref f-%data% (1) ((1 nfun)) f-%offset%)))
         label20))
     end_label
      (return (values nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::ftest fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (array double-float (*))
                        (fortran-to-lisp::integer4) (array double-float (*)))
           :return-values '(nil nil nil nil)
           :calls 'nil)))

