;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(let ((dim3g (make-array 14 :element-type 'double-float))
      (dim3w (make-array 65 :element-type 'double-float)))
  (declare (type (array double-float (14)) dim3g)
           (type (array double-float (65)) dim3w))
  (f2cl-lib:data-implied-do (((dim3g (i))) (i 1 14))
                            (((1 14)))
                            ((double-float))
                            (0.19d0 0.5d0 0.75d0 0.8d0 0.9949999999999999d0
                             0.99873449983514d0 0.7793703685672423d0
                             0.9999698993088767d0 0.7902637224771788d0
                             0.4403396687650737d0 0.4378478459006862d0
                             0.9549373822794593d0 0.9661093133630748d0
                             0.4577105877763134d0))
  (f2cl-lib:data-implied-do (((dim3w (i 1))) (i 1 13))
                            (((1 13) (1 5)))
                            ((double-float))
                            (0.007923078151105734d0 0.0679717739278808d0
                             0.001086986538805825d0 0.1838633662212829d0
                             0.03362119777829031d0 0.01013751123334062d0
                             0.001687648683985235d0 0.1346468564512807d0
                             0.001750145884600386d0 0.07752336383837453d0
                             0.2461864902770251d0 0.06797944868483038d0
                             0.01419962823300713d0))
  (f2cl-lib:data-implied-do (((dim3w (i 2))) (i 1 13))
                            (((1 13) (1 5)))
                            ((double-float))
                            (1.715006248224684d0 -0.3755893815889209d0
                             0.1488632145140549d0 -0.2497046640620823d0
                             0.1792501419135204d0 0.00344612675897389d0
                             -0.005140483185555825d0 0.006536017839876425d0
                             -6.5134549392297d-4 -0.006304672433547204d0
                             0.01266959399788263d0 -0.005454241018647931d0
                             0.004826995274768427d0))
  (f2cl-lib:data-implied-do (((dim3w (i 3))) (i 1 13))
                            (((1 13) (1 5)))
                            ((double-float))
                            (1.936014978949526d0 -0.3673449403754268d0
                             0.02929778657898176d0 -0.1151883520260315d0
                             0.05086658220872218d0 0.04453911087786469d0
                             -0.022878282571259d0 0.02908926216345833d0
                             -0.002898884350669207d0 -0.02805963413307495d0
                             0.05638741361145884d0 -0.02427469611942451d0
                             0.02148307034182882d0))
  (f2cl-lib:data-implied-do (((dim3w (i 4))) (i 1 13))
                            (((1 13) (1 5)))
                            ((double-float))
                            (0.517082819560576d0 0.01445269144914044d0
                             -0.3601489663995932d0 0.3628307003418485d0
                             0.007148802650872729d0 -0.09222852896022966d0
                             0.01719339732471725d0 -0.102141653746035d0
                             -0.007504397861080493d0 0.01648362537726711d0
                             0.05234610158469334d0 0.01445432331613066d0
                             0.003019236275367777d0))
  (f2cl-lib:data-implied-do (((dim3w (i 5))) (i 1 13))
                            (((1 13) (1 5)))
                            ((double-float))
                            (2.05440450381852d0 0.0137775998849012d0
                             -0.576806291790441d0 0.03726835047700328d0
                             0.006814878939777219d0 0.05723169733851849d0
                             -0.04493018743811285d0 0.02729236573866348d0
                             3.54747395055699d-4 0.01571366799739551d0
                             0.04990099219278567d0 0.0137791555266677d0
                             0.002878206423099872d0))
  (defun d113re (wtleng w g errcof rulpts)
    (declare (type (array double-float (*)) errcof)
             (type (array double-float (*)) rulpts g w)
             (type (f2cl-lib:integer4) wtleng))
    (f2cl-lib:with-multi-array-data
        ((w double-float w-%data% w-%offset%)
         (g double-float g-%data% g-%offset%)
         (rulpts double-float rulpts-%data% rulpts-%offset%)
         (errcof double-float errcof-%data% errcof-%offset%))
      (prog ((i 0) (j 0))
        (declare (type (f2cl-lib:integer4) j i))
        (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                      ((> i 13) nil)
          (tagbody
            (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                          ((> j 5) nil)
              (tagbody
                (setf (f2cl-lib:fref w-%data%
                                     (j i)
                                     ((1 5) (1 wtleng))
                                     w-%offset%)
                        (f2cl-lib:fref dim3w (i j) ((1 13) (1 5))))
               label10))))
       label10
        (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                      ((> i 3) nil)
          (tagbody
            (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                          ((> j 13) nil)
              (tagbody
                (setf (f2cl-lib:fref g-%data%
                                     (i j)
                                     ((1 3) (1 wtleng))
                                     g-%offset%)
                        (coerce (the f2cl-lib:integer4 0) 'double-float))
               label20))))
       label20
        (setf (f2cl-lib:fref g-%data% (1 2) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (1) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (1 3) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (2) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (1 4) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (3) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (1 5) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (4) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (1 6) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (5) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (1 7) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (6) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (2 7) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 7) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 8) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (7) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (2 8) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 8) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 9) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (8) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (2 9) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 9) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (3 9) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 9) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 10) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (9) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (2 10) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 10) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (3 10) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 10) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 11) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (10) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (2 11) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 11) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (3 11) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 11) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 12) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (12) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (2 12) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (11) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (3 12) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (2 12) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 13) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (13) ((1 14))))
        (setf (f2cl-lib:fref g-%data% (2 13) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 13) ((1 3) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (3 13) ((1 3) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim3g (14) ((1 14))))
        (setf (f2cl-lib:fref rulpts-%data% (1) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 1) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (2) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 6) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (3) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 6) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (4) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 6) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (5) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 6) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (6) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 6) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (7) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 12) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (8) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 12) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (9) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 8) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (10) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 8) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (11) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 8) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (12) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 24) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (13) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 24) 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (1) ((1 6)) errcof-%offset%)
                (coerce (the f2cl-lib:integer4 4) 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (2) ((1 6)) errcof-%offset%)
                (coerce 4.0 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (3) ((1 6)) errcof-%offset%)
                (coerce 0.5 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (4) ((1 6)) errcof-%offset%)
                (coerce 3.0 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (5) ((1 6)) errcof-%offset%)
                (coerce 0.5 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (6) ((1 6)) errcof-%offset%)
                (coerce 0.25 'double-float))
        (go end_label)
       end_label
        (return (values nil nil nil nil nil))))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::d113re
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)))
           :return-values '(nil nil nil nil nil)
           :calls 'nil)))

