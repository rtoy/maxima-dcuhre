;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':simple-array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(let* ((mdiv 1)
       (maxdim 15)
       (maxwt 14)
       (lenw2
        (+ (+ (* (* (* 2 mdiv) maxdim) (+ maxwt 1)) (* 12 maxwt))
           (* 2 maxdim))))
  (declare (type (f2cl-lib:integer4 1 1) mdiv)
           (type (f2cl-lib:integer4 15 15) maxdim)
           (type (f2cl-lib:integer4 14 14) maxwt)
           (type (f2cl-lib:integer4) lenw2)
           (ignorable mdiv maxdim maxwt lenw2))
  (defun dcuhre
         (ndim numfun a b minpts maxpts funsub epsabs epsrel key nw restar
          result abserr neval ifail work)
    (declare (type (double-float) epsrel epsabs)
             (type (simple-array double-float (*)) work abserr result b a)
             (type (f2cl-lib:integer4) ifail neval restar nw key maxpts minpts
                                       numfun ndim))
    (prog ((work2 (make-array lenw2 :element-type 'double-float)) (wrksub 0)
           (i1 0) (i2 0) (i3 0) (i4 0) (i5 0) (i6 0) (i7 0) (i8 0) (k1 0)
           (k2 0) (k3 0) (k4 0) (k5 0) (k6 0) (k7 0) (k8 0) (num 0) (nsub 0)
           (lenw 0) (keyf 0) (wtleng 0) (maxsub 0) (minsub 0))
      (declare (type (simple-array double-float (*)) work2)
               (type (f2cl-lib:integer4) wrksub i1 i2 i3 i4 i5 i6 i7 i8 k1 k2
                                         k3 k4 k5 k6 k7 k8 num nsub lenw keyf
                                         wtleng maxsub minsub))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9 var-10
             var-11 var-12 var-13 var-14 var-15 var-16 var-17 var-18)
          (dchhre maxdim ndim numfun mdiv a b minpts maxpts epsabs epsrel key
           nw restar num maxsub minsub keyf ifail wtleng)
        (declare (ignore var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8
                         var-9 var-10 var-11 var-12))
        (setf num var-13)
        (setf maxsub var-14)
        (setf minsub var-15)
        (setf keyf var-16)
        (setf ifail var-17)
        (setf wtleng var-18))
      (setf wrksub
              (the f2cl-lib:integer4
                   (truncate (+ (- nw 1) (* -17 mdiv numfun))
                             (+ (* 2 ndim) (* 2 numfun) 2))))
      (cond
        ((/= ifail 0)
         (go label999)))
      (setf i1 1)
      (setf i2 (f2cl-lib:int-add i1 (f2cl-lib:int-mul wrksub numfun)))
      (setf i3 (f2cl-lib:int-add i2 (f2cl-lib:int-mul wrksub numfun)))
      (setf i4 (f2cl-lib:int-add i3 (f2cl-lib:int-mul wrksub ndim)))
      (setf i5 (f2cl-lib:int-add i4 (f2cl-lib:int-mul wrksub ndim)))
      (setf i6 (f2cl-lib:int-add i5 wrksub))
      (setf i7 (f2cl-lib:int-add i6 wrksub))
      (setf i8 (f2cl-lib:int-add i7 (f2cl-lib:int-mul numfun mdiv)))
      (setf k1 1)
      (setf k2 (f2cl-lib:int-add k1 (f2cl-lib:int-mul 2 mdiv wtleng ndim)))
      (setf k3 (f2cl-lib:int-add k2 (f2cl-lib:int-mul wtleng 5)))
      (setf k4 (f2cl-lib:int-add k3 wtleng))
      (setf k5 (f2cl-lib:int-add k4 ndim))
      (setf k6 (f2cl-lib:int-add k5 ndim))
      (setf k7 (f2cl-lib:int-add k6 (f2cl-lib:int-mul 2 mdiv ndim)))
      (setf k8 (f2cl-lib:int-add k7 (f2cl-lib:int-mul 3 wtleng)))
      (cond
        ((= restar 1)
         (setf nsub (f2cl-lib:int (f2cl-lib:fref work (nw) ((1 nw)))))))
      (setf lenw (f2cl-lib:int-mul 16 mdiv numfun))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9 var-10
             var-11 var-12 var-13 var-14 var-15 var-16 var-17 var-18 var-19
             var-20 var-21 var-22 var-23 var-24 var-25 var-26 var-27 var-28
             var-29 var-30 var-31 var-32 var-33 var-34 var-35)
          (dadhre ndim numfun mdiv a b minsub maxsub funsub epsabs epsrel keyf
           restar num lenw wtleng result abserr neval nsub ifail
           (f2cl-lib:array-slice work double-float (i1) ((1 nw)))
           (f2cl-lib:array-slice work double-float (i2) ((1 nw)))
           (f2cl-lib:array-slice work double-float (i3) ((1 nw)))
           (f2cl-lib:array-slice work double-float (i4) ((1 nw)))
           (f2cl-lib:array-slice work double-float (i5) ((1 nw)))
           (f2cl-lib:array-slice work double-float (i6) ((1 nw)))
           (f2cl-lib:array-slice work double-float (i7) ((1 nw)))
           (f2cl-lib:array-slice work double-float (i8) ((1 nw)))
           (f2cl-lib:array-slice work2 double-float (k1) ((1 lenw2)))
           (f2cl-lib:array-slice work2 double-float (k2) ((1 lenw2)))
           (f2cl-lib:array-slice work2 double-float (k3) ((1 lenw2)))
           (f2cl-lib:array-slice work2 double-float (k4) ((1 lenw2)))
           (f2cl-lib:array-slice work2 double-float (k5) ((1 lenw2)))
           (f2cl-lib:array-slice work2 double-float (k6) ((1 lenw2)))
           (f2cl-lib:array-slice work2 double-float (k7) ((1 lenw2)))
           (f2cl-lib:array-slice work2 double-float (k8) ((1 lenw2))))
        (declare (ignore var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9 var-10
                         var-11 var-12 var-13 var-14 var-15 var-16 var-20
                         var-21 var-22 var-23 var-24 var-25 var-26 var-27
                         var-28 var-29 var-30 var-31 var-32 var-33 var-34
                         var-35))
        (setf ndim var-0)
        (setf numfun var-1)
        (setf neval var-17)
        (setf nsub var-18)
        (setf ifail var-19))
      (setf (f2cl-lib:fref work (nw) ((1 nw)))
              (coerce (the f2cl-lib:integer4 nsub) 'double-float))
     label999
      (go end_label)
     end_label
      (return
       (values ndim
               numfun
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               neval
               ifail
               nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::dcuhre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (simple-array double-float (*))
                        (simple-array double-float (*))
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        t (double-float) (double-float)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4)
                        (simple-array double-float (*))
                        (simple-array double-float (*))
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (simple-array double-float (*)))
           :return-values '(fortran-to-lisp::ndim fortran-to-lisp::numfun nil
                            nil nil nil nil nil nil nil nil nil nil nil
                            fortran-to-lisp::neval fortran-to-lisp::ifail nil)
           :calls '(fortran-to-lisp::dadhre fortran-to-lisp::dchhre))))

