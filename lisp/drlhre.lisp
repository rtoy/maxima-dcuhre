;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun drlhre
       (ndim center hwidth wtleng g w errcof numfun funsub scales norms x null$
        basval rgnerr direct)
  (declare (type (double-float) direct)
           (type (array double-float (*)) errcof)
           (type (array double-float (*)) rgnerr basval null$ x norms scales w
                                          g hwidth center)
           (type (f2cl-lib:integer4) numfun wtleng ndim))
  (f2cl-lib:with-multi-array-data
      ((center double-float center-%data% center-%offset%)
       (hwidth double-float hwidth-%data% hwidth-%offset%)
       (g double-float g-%data% g-%offset%)
       (w double-float w-%data% w-%offset%)
       (scales double-float scales-%data% scales-%offset%)
       (norms double-float norms-%data% norms-%offset%)
       (x double-float x-%data% x-%offset%)
       (null$ double-float null$-%data% null$-%offset%)
       (basval double-float basval-%data% basval-%offset%)
       (rgnerr double-float rgnerr-%data% rgnerr-%offset%)
       (errcof double-float errcof-%data% errcof-%offset%))
    (prog ((search$ 0.0d0) (ratio 0.0d0) (i 0) (j 0) (k 0) (divaxn 0)
           (rgnvol 0.0d0) (difsum 0.0d0) (difmax 0.0d0) (frthdf 0.0d0))
      (declare (type (f2cl-lib:integer4) divaxn k j i)
               (type (double-float) frthdf difmax difsum rgnvol ratio search$))
      (setf rgnvol (coerce (the f2cl-lib:integer4 1) 'double-float))
      (setf divaxn 1)
      (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                    ((> i ndim) nil)
        (tagbody
          (setf rgnvol
                  (* rgnvol
                     (f2cl-lib:fref hwidth-%data%
                                    (i)
                                    ((1 ndim))
                                    hwidth-%offset%)))
          (setf (f2cl-lib:fref x-%data% (i) ((1 ndim)) x-%offset%)
                  (f2cl-lib:fref center-%data% (i) ((1 ndim)) center-%offset%))
          (if
           (> (f2cl-lib:fref hwidth-%data% (i) ((1 ndim)) hwidth-%offset%)
              (f2cl-lib:fref hwidth-%data%
                             (divaxn)
                             ((1 ndim))
                             hwidth-%offset%))
           (setf divaxn i))
         label10))
      (multiple-value-bind (var-0 var-1 var-2 var-3)
          (funcall funsub ndim x numfun rgnerr)
        (declare (ignore var-1 var-3))
        (when var-0
          (setf ndim var-0))
        (when var-2
          (setf numfun var-2)))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref basval-%data% (j) ((1 numfun)) basval-%offset%)
                  (*
                   (f2cl-lib:fref w-%data% (1 1) ((1 5) (1 wtleng)) w-%offset%)
                   (f2cl-lib:fref rgnerr-%data%
                                  (j)
                                  ((1 numfun))
                                  rgnerr-%offset%)))
          (f2cl-lib:fdo (k 1 (f2cl-lib:int-add k 1))
                        ((> k 4) nil)
            (tagbody
              (setf (f2cl-lib:fref null$-%data%
                                   (j k)
                                   ((1 numfun) (1 8))
                                   null$-%offset%)
                      (*
                       (f2cl-lib:fref w-%data%
                                      ((f2cl-lib:int-add k 1) 1)
                                      ((1 5) (1 wtleng))
                                      w-%offset%)
                       (f2cl-lib:fref rgnerr-%data%
                                      (j)
                                      ((1 numfun))
                                      rgnerr-%offset%)))
             label20))
         label30))
      (setf difmax (coerce (the f2cl-lib:integer4 0) 'double-float))
      (setf ratio
              (expt
               (/
                (f2cl-lib:fref g-%data% (1 3) ((1 ndim) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data%
                               (1 2)
                               ((1 ndim) (1 wtleng))
                               g-%offset%))
               2))
      (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                    ((> i ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref x-%data% (i) ((1 ndim)) x-%offset%)
                  (-
                   (f2cl-lib:fref center-%data% (i) ((1 ndim)) center-%offset%)
                   (*
                    (f2cl-lib:fref hwidth-%data%
                                   (i)
                                   ((1 ndim))
                                   hwidth-%offset%)
                    (f2cl-lib:fref g-%data%
                                   (1 2)
                                   ((1 ndim) (1 wtleng))
                                   g-%offset%))))
          (multiple-value-bind (var-0 var-1 var-2 var-3)
              (funcall funsub
                       ndim
                       x
                       numfun
                       (f2cl-lib:array-slice null$-%data%
                                             double-float
                                             (1 5)
                                             ((1 numfun) (1 8))
                                             null$-%offset%))
            (declare (ignore var-1 var-3))
            (when var-0
              (setf ndim var-0))
            (when var-2
              (setf numfun var-2)))
          (setf (f2cl-lib:fref x-%data% (i) ((1 ndim)) x-%offset%)
                  (+
                   (f2cl-lib:fref center-%data% (i) ((1 ndim)) center-%offset%)
                   (*
                    (f2cl-lib:fref hwidth-%data%
                                   (i)
                                   ((1 ndim))
                                   hwidth-%offset%)
                    (f2cl-lib:fref g-%data%
                                   (1 2)
                                   ((1 ndim) (1 wtleng))
                                   g-%offset%))))
          (multiple-value-bind (var-0 var-1 var-2 var-3)
              (funcall funsub
                       ndim
                       x
                       numfun
                       (f2cl-lib:array-slice null$-%data%
                                             double-float
                                             (1 6)
                                             ((1 numfun) (1 8))
                                             null$-%offset%))
            (declare (ignore var-1 var-3))
            (when var-0
              (setf ndim var-0))
            (when var-2
              (setf numfun var-2)))
          (setf (f2cl-lib:fref x-%data% (i) ((1 ndim)) x-%offset%)
                  (-
                   (f2cl-lib:fref center-%data% (i) ((1 ndim)) center-%offset%)
                   (*
                    (f2cl-lib:fref hwidth-%data%
                                   (i)
                                   ((1 ndim))
                                   hwidth-%offset%)
                    (f2cl-lib:fref g-%data%
                                   (1 3)
                                   ((1 ndim) (1 wtleng))
                                   g-%offset%))))
          (multiple-value-bind (var-0 var-1 var-2 var-3)
              (funcall funsub
                       ndim
                       x
                       numfun
                       (f2cl-lib:array-slice null$-%data%
                                             double-float
                                             (1 7)
                                             ((1 numfun) (1 8))
                                             null$-%offset%))
            (declare (ignore var-1 var-3))
            (when var-0
              (setf ndim var-0))
            (when var-2
              (setf numfun var-2)))
          (setf (f2cl-lib:fref x-%data% (i) ((1 ndim)) x-%offset%)
                  (+
                   (f2cl-lib:fref center-%data% (i) ((1 ndim)) center-%offset%)
                   (*
                    (f2cl-lib:fref hwidth-%data%
                                   (i)
                                   ((1 ndim))
                                   hwidth-%offset%)
                    (f2cl-lib:fref g-%data%
                                   (1 3)
                                   ((1 ndim) (1 wtleng))
                                   g-%offset%))))
          (multiple-value-bind (var-0 var-1 var-2 var-3)
              (funcall funsub
                       ndim
                       x
                       numfun
                       (f2cl-lib:array-slice null$-%data%
                                             double-float
                                             (1 8)
                                             ((1 numfun) (1 8))
                                             null$-%offset%))
            (declare (ignore var-1 var-3))
            (when var-0
              (setf ndim var-0))
            (when var-2
              (setf numfun var-2)))
          (setf (f2cl-lib:fref x-%data% (i) ((1 ndim)) x-%offset%)
                  (f2cl-lib:fref center-%data% (i) ((1 ndim)) center-%offset%))
          (setf difsum (coerce (the f2cl-lib:integer4 0) 'double-float))
          (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                        ((> j numfun) nil)
            (tagbody
              (setf frthdf
                      (+
                       (-
                        (* 2
                           (- 1 ratio)
                           (f2cl-lib:fref rgnerr-%data%
                                          (j)
                                          ((1 numfun))
                                          rgnerr-%offset%))
                        (+
                         (f2cl-lib:fref null$-%data%
                                        (j 7)
                                        ((1 numfun) (1 8))
                                        null$-%offset%)
                         (f2cl-lib:fref null$-%data%
                                        (j 8)
                                        ((1 numfun) (1 8))
                                        null$-%offset%)))
                       (* ratio
                          (+
                           (f2cl-lib:fref null$-%data%
                                          (j 5)
                                          ((1 numfun) (1 8))
                                          null$-%offset%)
                           (f2cl-lib:fref null$-%data%
                                          (j 6)
                                          ((1 numfun) (1 8))
                                          null$-%offset%)))))
              (if
               (/=
                (+
                 (f2cl-lib:fref rgnerr-%data% (j) ((1 numfun)) rgnerr-%offset%)
                 (/ frthdf 4))
                (f2cl-lib:fref rgnerr-%data% (j) ((1 numfun)) rgnerr-%offset%))
               (setf difsum (+ difsum (abs frthdf))))
              (f2cl-lib:fdo (k 1 (f2cl-lib:int-add k 1))
                            ((> k 4) nil)
                (tagbody
                  (setf (f2cl-lib:fref null$-%data%
                                       (j k)
                                       ((1 numfun) (1 8))
                                       null$-%offset%)
                          (+
                           (f2cl-lib:fref null$-%data%
                                          (j k)
                                          ((1 numfun) (1 8))
                                          null$-%offset%)
                           (*
                            (f2cl-lib:fref w-%data%
                                           ((f2cl-lib:int-add k 1) 2)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            (+
                             (f2cl-lib:fref null$-%data%
                                            (j 5)
                                            ((1 numfun) (1 8))
                                            null$-%offset%)
                             (f2cl-lib:fref null$-%data%
                                            (j 6)
                                            ((1 numfun) (1 8))
                                            null$-%offset%)))
                           (*
                            (f2cl-lib:fref w-%data%
                                           ((f2cl-lib:int-add k 1) 3)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            (+
                             (f2cl-lib:fref null$-%data%
                                            (j 7)
                                            ((1 numfun) (1 8))
                                            null$-%offset%)
                             (f2cl-lib:fref null$-%data%
                                            (j 8)
                                            ((1 numfun) (1 8))
                                            null$-%offset%)))))
                 label40))
              (setf (f2cl-lib:fref basval-%data%
                                   (j)
                                   ((1 numfun))
                                   basval-%offset%)
                      (+
                       (f2cl-lib:fref basval-%data%
                                      (j)
                                      ((1 numfun))
                                      basval-%offset%)
                       (*
                        (f2cl-lib:fref w-%data%
                                       (1 2)
                                       ((1 5) (1 wtleng))
                                       w-%offset%)
                        (+
                         (f2cl-lib:fref null$-%data%
                                        (j 5)
                                        ((1 numfun) (1 8))
                                        null$-%offset%)
                         (f2cl-lib:fref null$-%data%
                                        (j 6)
                                        ((1 numfun) (1 8))
                                        null$-%offset%)))
                       (*
                        (f2cl-lib:fref w-%data%
                                       (1 3)
                                       ((1 5) (1 wtleng))
                                       w-%offset%)
                        (+
                         (f2cl-lib:fref null$-%data%
                                        (j 7)
                                        ((1 numfun) (1 8))
                                        null$-%offset%)
                         (f2cl-lib:fref null$-%data%
                                        (j 8)
                                        ((1 numfun) (1 8))
                                        null$-%offset%)))))
             label50))
          (cond
            ((> difsum difmax)
             (setf difmax difsum)
             (setf divaxn i)))
         label60))
      (setf direct (coerce (the f2cl-lib:integer4 divaxn) 'double-float))
      (f2cl-lib:fdo (i 4 (f2cl-lib:int-add i 1))
                    ((> i wtleng) nil)
        (tagbody
          (multiple-value-bind
                (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8)
              (dfshre ndim center hwidth x
               (f2cl-lib:array-slice g-%data%
                                     double-float
                                     (1 i)
                                     ((1 ndim) (1 wtleng))
                                     g-%offset%)
               numfun funsub rgnerr
               (f2cl-lib:array-slice null$-%data%
                                     double-float
                                     (1 5)
                                     ((1 numfun) (1 8))
                                     null$-%offset%))
            (declare (ignore var-1 var-2 var-3 var-4 var-6 var-7 var-8))
            (setf ndim var-0)
            (setf numfun var-5))
          (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                        ((> j numfun) nil)
            (tagbody
              (setf (f2cl-lib:fref basval-%data%
                                   (j)
                                   ((1 numfun))
                                   basval-%offset%)
                      (+
                       (f2cl-lib:fref basval-%data%
                                      (j)
                                      ((1 numfun))
                                      basval-%offset%)
                       (*
                        (f2cl-lib:fref w-%data%
                                       (1 i)
                                       ((1 5) (1 wtleng))
                                       w-%offset%)
                        (f2cl-lib:fref rgnerr-%data%
                                       (j)
                                       ((1 numfun))
                                       rgnerr-%offset%))))
              (f2cl-lib:fdo (k 1 (f2cl-lib:int-add k 1))
                            ((> k 4) nil)
                (tagbody
                  (setf (f2cl-lib:fref null$-%data%
                                       (j k)
                                       ((1 numfun) (1 8))
                                       null$-%offset%)
                          (+
                           (f2cl-lib:fref null$-%data%
                                          (j k)
                                          ((1 numfun) (1 8))
                                          null$-%offset%)
                           (*
                            (f2cl-lib:fref w-%data%
                                           ((f2cl-lib:int-add k 1) i)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            (f2cl-lib:fref rgnerr-%data%
                                           (j)
                                           ((1 numfun))
                                           rgnerr-%offset%))))
                 label70))
             label80))
         label90))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                        ((> i 3) nil)
            (tagbody
              (setf search$ (coerce (the f2cl-lib:integer4 0) 'double-float))
              (f2cl-lib:fdo (k 1 (f2cl-lib:int-add k 1))
                            ((> k wtleng) nil)
                (tagbody
                  (setf search$
                          (max search$
                               (*
                                (abs
                                 (+
                                  (f2cl-lib:fref null$-%data%
                                                 (j (f2cl-lib:int-add i 1))
                                                 ((1 numfun) (1 8))
                                                 null$-%offset%)
                                  (*
                                   (f2cl-lib:fref scales-%data%
                                                  (i k)
                                                  ((1 3) (1 wtleng))
                                                  scales-%offset%)
                                   (f2cl-lib:fref null$-%data%
                                                  (j i)
                                                  ((1 numfun) (1 8))
                                                  null$-%offset%))))
                                (f2cl-lib:fref norms-%data%
                                               (i k)
                                               ((1 3) (1 wtleng))
                                               norms-%offset%))))
                 label100))
              (setf (f2cl-lib:fref null$-%data%
                                   (j i)
                                   ((1 numfun) (1 8))
                                   null$-%offset%)
                      search$)
             label110))
          (cond
            ((and
              (<=
               (* (f2cl-lib:fref errcof (1) ((1 6)))
                  (f2cl-lib:fref null$ (j 1) ((1 numfun) (1 8))))
               (f2cl-lib:fref null$ (j 2) ((1 numfun) (1 8))))
              (<=
               (* (f2cl-lib:fref errcof (2) ((1 6)))
                  (f2cl-lib:fref null$ (j 2) ((1 numfun) (1 8))))
               (f2cl-lib:fref null$ (j 3) ((1 numfun) (1 8)))))
             (setf (f2cl-lib:fref rgnerr-%data%
                                  (j)
                                  ((1 numfun))
                                  rgnerr-%offset%)
                     (*
                      (f2cl-lib:fref errcof-%data% (3) ((1 6)) errcof-%offset%)
                      (f2cl-lib:fref null$-%data%
                                     (j 1)
                                     ((1 numfun) (1 8))
                                     null$-%offset%))))
            (t
             (setf (f2cl-lib:fref rgnerr-%data%
                                  (j)
                                  ((1 numfun))
                                  rgnerr-%offset%)
                     (*
                      (f2cl-lib:fref errcof-%data% (4) ((1 6)) errcof-%offset%)
                      (max
                       (f2cl-lib:fref null$-%data%
                                      (j 1)
                                      ((1 numfun) (1 8))
                                      null$-%offset%)
                       (f2cl-lib:fref null$-%data%
                                      (j 2)
                                      ((1 numfun) (1 8))
                                      null$-%offset%)
                       (f2cl-lib:fref null$-%data%
                                      (j 3)
                                      ((1 numfun) (1 8))
                                      null$-%offset%))))))
          (setf (f2cl-lib:fref rgnerr-%data% (j) ((1 numfun)) rgnerr-%offset%)
                  (* rgnvol
                     (f2cl-lib:fref rgnerr-%data%
                                    (j)
                                    ((1 numfun))
                                    rgnerr-%offset%)))
          (setf (f2cl-lib:fref basval-%data% (j) ((1 numfun)) basval-%offset%)
                  (* rgnvol
                     (f2cl-lib:fref basval-%data%
                                    (j)
                                    ((1 numfun))
                                    basval-%offset%)))
         label130))
     end_label
      (return
       (values ndim
               nil
               nil
               nil
               nil
               nil
               nil
               numfun
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               direct)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::drlhre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (array double-float (*))
                        (array double-float (*)) (fortran-to-lisp::integer4)
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (fortran-to-lisp::integer4) t
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (double-float))
           :return-values '(fortran-to-lisp::ndim nil nil nil nil nil nil
                            fortran-to-lisp::numfun nil nil nil nil nil nil nil
                            fortran-to-lisp::direct)
           :calls '(fortran-to-lisp::dfshre))))

