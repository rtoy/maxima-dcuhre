;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun dfshre (ndim center hwidth x g numfun funsub fulsms funvls)
  (declare (type (array double-float (*)) funvls fulsms g x hwidth center)
           (type (f2cl-lib:integer4) numfun ndim))
  (f2cl-lib:with-multi-array-data
      ((center double-float center-%data% center-%offset%)
       (hwidth double-float hwidth-%data% hwidth-%offset%)
       (x double-float x-%data% x-%offset%)
       (g double-float g-%data% g-%offset%)
       (fulsms double-float fulsms-%data% fulsms-%offset%)
       (funvls double-float funvls-%data% funvls-%offset%))
    (prog ((gl 0.0d0) (gi 0.0d0) (ixchng 0) (lxchng 0) (i 0) (j 0) (l 0))
      (declare (type (f2cl-lib:integer4) l j i lxchng ixchng)
               (type (double-float) gi gl))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref fulsms-%data% (j) ((1 numfun)) fulsms-%offset%)
                  (coerce (the f2cl-lib:integer4 0) 'double-float))
         label10))
     label20
      (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                    ((> i ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref x-%data% (i) ((1 ndim)) x-%offset%)
                  (+
                   (f2cl-lib:fref center-%data% (i) ((1 ndim)) center-%offset%)
                   (* (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%)
                      (f2cl-lib:fref hwidth-%data%
                                     (i)
                                     ((1 ndim))
                                     hwidth-%offset%))))
         label30))
     label40
      (multiple-value-bind (var-0 var-1 var-2 var-3)
          (funcall funsub ndim x numfun funvls)
        (declare (ignore var-1 var-3))
        (when var-0
          (setf ndim var-0))
        (when var-2
          (setf numfun var-2)))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref fulsms-%data% (j) ((1 numfun)) fulsms-%offset%)
                  (+
                   (f2cl-lib:fref fulsms-%data%
                                  (j)
                                  ((1 numfun))
                                  fulsms-%offset%)
                   (f2cl-lib:fref funvls-%data%
                                  (j)
                                  ((1 numfun))
                                  funvls-%offset%)))
         label50))
      (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                    ((> i ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%)
                  (- (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%)))
          (setf (f2cl-lib:fref x-%data% (i) ((1 ndim)) x-%offset%)
                  (+
                   (f2cl-lib:fref center-%data% (i) ((1 ndim)) center-%offset%)
                   (* (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%)
                      (f2cl-lib:fref hwidth-%data%
                                     (i)
                                     ((1 ndim))
                                     hwidth-%offset%))))
          (if (< (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%) 0)
              (go label40))
         label60))
      (f2cl-lib:fdo (i 2 (f2cl-lib:int-add i 1))
                    ((> i ndim) nil)
        (tagbody
          (cond
            ((>
              (f2cl-lib:fref g
                             ((f2cl-lib:int-add i (f2cl-lib:int-sub 1)))
                             ((1 ndim)))
              (f2cl-lib:fref g (i) ((1 ndim))))
             (setf gi (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%))
             (setf ixchng (f2cl-lib:int-sub i 1))
             (f2cl-lib:fdo (l 1 (f2cl-lib:int-add l 1))
                           ((> l
                               (f2cl-lib:f2cl/
                                (f2cl-lib:int-add i (f2cl-lib:int-sub 1))
                                2))
                            nil)
               (tagbody
                 (setf gl (f2cl-lib:fref g-%data% (l) ((1 ndim)) g-%offset%))
                 (setf (f2cl-lib:fref g-%data% (l) ((1 ndim)) g-%offset%)
                         (f2cl-lib:fref g-%data%
                                        ((f2cl-lib:int-sub i l))
                                        ((1 ndim))
                                        g-%offset%))
                 (setf (f2cl-lib:fref g-%data%
                                      ((f2cl-lib:int-sub i l))
                                      ((1 ndim))
                                      g-%offset%)
                         gl)
                 (if (<= gl gi) (setf ixchng (f2cl-lib:int-sub ixchng 1)))
                 (if (> (f2cl-lib:fref g-%data% (l) ((1 ndim)) g-%offset%) gi)
                     (setf lxchng l))
                label70))
             (if
              (<= (f2cl-lib:fref g-%data% (ixchng) ((1 ndim)) g-%offset%) gi)
              (setf ixchng lxchng))
             (setf (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%)
                     (f2cl-lib:fref g-%data% (ixchng) ((1 ndim)) g-%offset%))
             (setf (f2cl-lib:fref g-%data% (ixchng) ((1 ndim)) g-%offset%) gi)
             (go label20)))
         label80))
      (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                    ((> i (f2cl-lib:f2cl/ ndim 2)) nil)
        (tagbody
          (setf gi (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%))
          (setf (f2cl-lib:fref g-%data% (i) ((1 ndim)) g-%offset%)
                  (f2cl-lib:fref g-%data%
                                 ((f2cl-lib:int-add (f2cl-lib:int-sub ndim i)
                                                    1))
                                 ((1 ndim))
                                 g-%offset%))
          (setf (f2cl-lib:fref g-%data%
                               ((f2cl-lib:int-add (f2cl-lib:int-sub ndim i) 1))
                               ((1 ndim))
                               g-%offset%)
                  gi)
         label90))
     end_label
      (return (values ndim nil nil nil nil numfun nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::dfshre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (fortran-to-lisp::integer4) t
                        (array double-float (*)) (array double-float (*)))
           :return-values '(fortran-to-lisp::ndim nil nil nil nil
                            fortran-to-lisp::numfun nil nil nil)
           :calls 'nil)))

