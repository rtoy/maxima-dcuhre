;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun dinhre (ndim key wtleng w g errcof rulpts scales norms)
  (declare (type (array double-float (*)) errcof)
           (type (array double-float (*)) norms scales rulpts g w)
           (type (f2cl-lib:integer4) wtleng key ndim))
  (f2cl-lib:with-multi-array-data
      ((w double-float w-%data% w-%offset%)
       (g double-float g-%data% g-%offset%)
       (rulpts double-float rulpts-%data% rulpts-%offset%)
       (scales double-float scales-%data% scales-%offset%)
       (norms double-float norms-%data% norms-%offset%)
       (errcof double-float errcof-%data% errcof-%offset%))
    (prog ((we (make-array 14 :element-type 'double-float)) (i 0) (j 0) (k 0))
      (declare (type (f2cl-lib:integer4) k j i)
               (type (array double-float (14)) we))
      (cond
        ((= key 1)
         (d132re wtleng w g errcof rulpts))
        ((= key 2)
         (d113re wtleng w g errcof rulpts))
        ((= key 3)
         (d09hre ndim wtleng w g errcof rulpts))
        ((= key 4)
         (d07hre ndim wtleng w g errcof rulpts)))
      (f2cl-lib:fdo (k 1 (f2cl-lib:int-add k 1))
                    ((> k 3) nil)
        (tagbody
          (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                        ((> i wtleng) nil)
            (tagbody
              (cond
                ((/=
                  (f2cl-lib:fref w
                                 ((f2cl-lib:int-add k 1) i)
                                 ((1 5) (1 wtleng)))
                  0)
                 (setf (f2cl-lib:fref scales-%data%
                                      (k i)
                                      ((1 3) (1 wtleng))
                                      scales-%offset%)
                         (/
                          (-
                           (f2cl-lib:fref w-%data%
                                          ((f2cl-lib:int-add k 2) i)
                                          ((1 5) (1 wtleng))
                                          w-%offset%))
                          (f2cl-lib:fref w-%data%
                                         ((f2cl-lib:int-add k 1) i)
                                         ((1 5) (1 wtleng))
                                         w-%offset%))))
                (t
                 (setf (f2cl-lib:fref scales-%data%
                                      (k i)
                                      ((1 3) (1 wtleng))
                                      scales-%offset%)
                         (coerce (the f2cl-lib:integer4 100) 'double-float))))
              (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                            ((> j wtleng) nil)
                (tagbody
                  (setf (f2cl-lib:fref we (j) ((1 14)))
                          (+
                           (f2cl-lib:fref w-%data%
                                          ((f2cl-lib:int-add k 2) j)
                                          ((1 5) (1 wtleng))
                                          w-%offset%)
                           (*
                            (f2cl-lib:fref scales-%data%
                                           (k i)
                                           ((1 3) (1 wtleng))
                                           scales-%offset%)
                            (f2cl-lib:fref w-%data%
                                           ((f2cl-lib:int-add k 1) j)
                                           ((1 5) (1 wtleng))
                                           w-%offset%))))
                 label30))
              (setf (f2cl-lib:fref norms-%data%
                                   (k i)
                                   ((1 3) (1 wtleng))
                                   norms-%offset%)
                      (coerce (the f2cl-lib:integer4 0) 'double-float))
              (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                            ((> j wtleng) nil)
                (tagbody
                  (setf (f2cl-lib:fref norms-%data%
                                       (k i)
                                       ((1 3) (1 wtleng))
                                       norms-%offset%)
                          (+
                           (f2cl-lib:fref norms-%data%
                                          (k i)
                                          ((1 3) (1 wtleng))
                                          norms-%offset%)
                           (*
                            (f2cl-lib:fref rulpts-%data%
                                           (j)
                                           ((1 wtleng))
                                           rulpts-%offset%)
                            (abs (f2cl-lib:fref we (j) ((1 14)))))))
                 label40))
              (setf (f2cl-lib:fref norms-%data%
                                   (k i)
                                   ((1 3) (1 wtleng))
                                   norms-%offset%)
                      (/ (expt 2 ndim)
                         (f2cl-lib:fref norms-%data%
                                        (k i)
                                        ((1 3) (1 wtleng))
                                        norms-%offset%)))
             label50))
         label100))
      (go end_label)
     end_label
      (return (values nil nil nil nil nil nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::dinhre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)))
           :return-values '(nil nil nil nil nil nil nil nil nil)
           :calls '(fortran-to-lisp::d07hre fortran-to-lisp::d09hre
                    fortran-to-lisp::d113re fortran-to-lisp::d132re))))

