;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun ftestp (ndim z nfun f)
  (declare (type (array double-float (*)) f z)
           (type (f2cl-lib:integer4) nfun ndim))
  (f2cl-lib:with-multi-array-data
      ((z double-float z-%data% z-%offset%)
       (f double-float f-%data% f-%offset%))
    (prog ()
      (declare)
      (setf (f2cl-lib:fref f-%data% (1) ((1 nfun)) f-%offset%)
              (coerce (the f2cl-lib:integer4 1) 'double-float))
      (setf (f2cl-lib:fref f-%data% (2) ((1 nfun)) f-%offset%)
              (* 2 (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%)))
      (setf (f2cl-lib:fref f-%data% (3) ((1 nfun)) f-%offset%)
              (* 3
                 (expt (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%) 2)))
      (setf (f2cl-lib:fref f-%data% (4) ((1 nfun)) f-%offset%)
              (* (f2cl-lib:fref f-%data% (2) ((1 nfun)) f-%offset%)
                 2
                 (f2cl-lib:fref z-%data% (2) ((1 ndim)) z-%offset%)))
      (setf (f2cl-lib:fref f-%data% (5) ((1 nfun)) f-%offset%)
              (* 4
                 (expt (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%) 3)))
      (setf (f2cl-lib:fref f-%data% (6) ((1 nfun)) f-%offset%)
              (* (f2cl-lib:fref f-%data% (3) ((1 nfun)) f-%offset%)
                 2
                 (f2cl-lib:fref z-%data% (2) ((1 ndim)) z-%offset%)))
      (setf (f2cl-lib:fref f-%data% (7) ((1 nfun)) f-%offset%)
              (* 5
                 (expt (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%) 4)))
      (setf (f2cl-lib:fref f-%data% (8) ((1 nfun)) f-%offset%)
              (* (f2cl-lib:fref f-%data% (5) ((1 nfun)) f-%offset%)
                 2
                 (f2cl-lib:fref z-%data% (2) ((1 ndim)) z-%offset%)))
      (setf (f2cl-lib:fref f-%data% (9) ((1 nfun)) f-%offset%)
              (* (f2cl-lib:fref f-%data% (3) ((1 nfun)) f-%offset%)
                 3
                 (expt (f2cl-lib:fref z-%data% (2) ((1 ndim)) z-%offset%) 2)))
      (setf (f2cl-lib:fref f-%data% (10) ((1 nfun)) f-%offset%)
              (* 6
                 (expt (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%) 5)))
      (setf (f2cl-lib:fref f-%data% (11) ((1 nfun)) f-%offset%)
              (* (f2cl-lib:fref f-%data% (7) ((1 nfun)) f-%offset%)
                 2
                 (f2cl-lib:fref z-%data% (2) ((1 ndim)) z-%offset%)))
      (setf (f2cl-lib:fref f-%data% (12) ((1 nfun)) f-%offset%)
              (* (f2cl-lib:fref f-%data% (5) ((1 nfun)) f-%offset%)
                 3
                 (expt (f2cl-lib:fref z-%data% (2) ((1 ndim)) z-%offset%) 2)))
      (setf (f2cl-lib:fref f-%data% (13) ((1 nfun)) f-%offset%)
              (* 8
                 (expt (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%) 7)))
      (setf (f2cl-lib:fref f-%data% (14) ((1 nfun)) f-%offset%)
              (* 10
                 (expt (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%) 9)))
      (setf (f2cl-lib:fref f-%data% (15) ((1 nfun)) f-%offset%)
              (* 12
                 (expt (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%) 11)))
      (setf (f2cl-lib:fref f-%data% (16) ((1 nfun)) f-%offset%)
              (* 14
                 (expt (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%) 13)))
     end_label
      (return (values nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::ftestp
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (array double-float (*))
                        (fortran-to-lisp::integer4) (array double-float (*)))
           :return-values '(nil nil nil nil)
           :calls 'nil)))

;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun ftesto (ndim z nfun f)
  (declare (type (array double-float (*)) f z)
           (type (f2cl-lib:integer4) nfun ndim))
  (f2cl-lib:with-multi-array-data
      ((z double-float z-%data% z-%offset%)
       (f double-float f-%data% f-%offset%))
    (prog ()
      (declare)
      (setf (f2cl-lib:fref f-%data% (1) ((1 nfun)) f-%offset%)
              (/
               (/ 10
                  (expt
                   (+ 1
                      (* 0.1
                         (f2cl-lib:fref z-%data% (1) ((1 ndim)) z-%offset%))
                      (* 0.2
                         (f2cl-lib:fref z-%data% (2) ((1 ndim)) z-%offset%))
                      (* 0.3
                         (f2cl-lib:fref z-%data% (3) ((1 ndim)) z-%offset%))
                      (* 0.4
                         (f2cl-lib:fref z-%data% (4) ((1 ndim)) z-%offset%))
                      (* 0.5
                         (f2cl-lib:fref z-%data% (5) ((1 ndim)) z-%offset%))
                      (* 0.6
                         (f2cl-lib:fref z-%data% (6) ((1 ndim)) z-%offset%)))
                   6))
               0.2057746))
     end_label
      (return (values nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::ftesto
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (array double-float (*))
                        (fortran-to-lisp::integer4) (array double-float (*)))
           :return-values '(nil nil nil nil)
           :calls 'nil)))

;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun ftestx (ndim z nfun f)
  (declare (type (array double-float (*)) f z)
           (type (f2cl-lib:integer4) nfun ndim))
  (f2cl-lib:with-multi-array-data
      ((z double-float z-%data% z-%offset%)
       (f double-float f-%data% f-%offset%))
    (prog ((sum 0.0d0) (n 0))
      (declare (type (f2cl-lib:integer4) n) (type (double-float) sum))
      (setf sum (coerce (the f2cl-lib:integer4 0) 'double-float))
      (f2cl-lib:fdo (n 1 (f2cl-lib:int-add n 1))
                    ((> n 2) nil)
        (tagbody
          (setf sum
                  (+ sum
                     (/
                      (-
                       (cos
                        (* 10
                           (f2cl-lib:fref z-%data%
                                          (n)
                                          ((1 ndim))
                                          z-%offset%))))
                      0.05440211)))
         label10))
      (setf (f2cl-lib:fref f-%data% (1) ((1 nfun)) f-%offset%) (/ sum 2))
     end_label
      (return (values nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::ftestx
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (array double-float (*))
                        (fortran-to-lisp::integer4) (array double-float (*)))
           :return-values '(nil nil nil nil)
           :calls 'nil)))

