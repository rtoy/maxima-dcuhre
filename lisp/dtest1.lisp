;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':simple-array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(let* ((ndim 5) (nw 5000) (nf (+ ndim 1)))
  (declare (type (f2cl-lib:integer4 5 5) ndim)
           (type (f2cl-lib:integer4 5000 5000) nw)
           (type (f2cl-lib:integer4) nf)
           (ignorable ndim nw nf))
  (defun dtest1 ()
    (prog ((absest (make-array nf :element-type 'double-float))
           (finest (make-array nf :element-type 'double-float)) (absreq 0.0d0)
           (relreq 0.0d0) (a (make-array ndim :element-type 'double-float))
           (b (make-array ndim :element-type 'double-float))
           (wrkstr (make-array nw :element-type 'double-float)) (key 0) (n 0)
           (mincls 0) (maxcls 0) (ifail 0) (neval 0))
      (declare (type (double-float) absreq relreq)
               (type (simple-array double-float (*)) absest finest a b wrkstr)
               (type (f2cl-lib:integer4) key n mincls maxcls ifail neval))
      (f2cl-lib:fdo (n 1 (f2cl-lib:int-add n 1))
                    ((> n ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref a (n) ((1 ndim)))
                  (coerce (the f2cl-lib:integer4 0) 'double-float))
          (setf (f2cl-lib:fref b (n) ((1 ndim)))
                  (coerce (the f2cl-lib:integer4 1) 'double-float))
         label10))
      (setf mincls 0)
      (setf maxcls 10000)
      (setf key 0)
      (setf absreq (coerce (the f2cl-lib:integer4 0) 'double-float))
      (setf relreq (coerce 0.001 'double-float))
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9 var-10
             var-11 var-12 var-13 var-14 var-15 var-16)
          (dcuhre ndim nf a b mincls maxcls #'ftest absreq relreq key nw 0
           finest absest neval ifail wrkstr)
        (declare (ignore var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9 var-10
                         var-11 var-12 var-13 var-16))
        (setf ndim var-0)
        (setf nf var-1)
        (setf neval var-14)
        (setf ifail var-15))
      (f2cl-lib:fformat t
                        ("~8@T" "DCUHRE TEST RESULTS" "~%" "~%"
                         "     FTEST CALLS = " 1 (("~4D")) ", IFAIL = " 1
                         (("~2D")) "~%" "    N   ESTIMATED ERROR   INTEGRAL"
                         "~%")
                        neval
                        ifail)
      (f2cl-lib:fdo (n 1 (f2cl-lib:int-add n 1))
                    ((> n nf) nil)
        (tagbody
          (f2cl-lib:fformat t
                            ("~3@T" 1 (("~2D")) 2 (("~15,8,0,'*,F")) "~%")
                            n
                            (f2cl-lib:fref absest (n) ((1 nf)))
                            (f2cl-lib:fref finest (n) ((1 nf))))
         label20))
     end_label
      (return nil))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::dtest1
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo :arg-types 'nil
                                            :return-values 'nil
                                            :calls '(fortran-to-lisp::dcuhre))))

