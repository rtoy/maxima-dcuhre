;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun d09hre (ndim wtleng w g errcof rulpts)
  (declare (type (array double-float (*)) errcof)
           (type (array double-float (*)) rulpts g w)
           (type (f2cl-lib:integer4) wtleng ndim))
  (f2cl-lib:with-multi-array-data
      ((w double-float w-%data% w-%offset%)
       (g double-float g-%data% g-%offset%)
       (rulpts double-float rulpts-%data% rulpts-%offset%)
       (errcof double-float errcof-%data% errcof-%offset%))
    (prog ((i 0) (j 0) (ratio 0.0d0) (lam0 0.0d0) (lam1 0.0d0) (lam2 0.0d0)
           (lam3 0.0d0) (lamp 0.0d0) (twondm 0.0d0))
      (declare (type (double-float) twondm lamp lam3 lam2 lam1 lam0 ratio)
               (type (f2cl-lib:integer4) j i))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j wtleng) nil)
        (tagbody
          (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                        ((> i ndim) nil)
            (tagbody
              (setf (f2cl-lib:fref g-%data%
                                   (i j)
                                   ((1 ndim) (1 wtleng))
                                   g-%offset%)
                      (coerce (the f2cl-lib:integer4 0) 'double-float))
             label10))
          (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                        ((> i 5) nil)
            (tagbody
              (setf (f2cl-lib:fref w-%data%
                                   (i j)
                                   ((1 5) (1 wtleng))
                                   w-%offset%)
                      (coerce (the f2cl-lib:integer4 0) 'double-float))
             label20))
          (setf (f2cl-lib:fref rulpts-%data% (j) ((1 wtleng)) rulpts-%offset%)
                  (coerce (the f2cl-lib:integer4 (f2cl-lib:int-mul 2 ndim))
                          'double-float))
         label30))
      (setf twondm
              (coerce (the f2cl-lib:integer4 (expt 2 ndim)) 'double-float))
      (setf (f2cl-lib:fref rulpts-%data% (wtleng) ((1 wtleng)) rulpts-%offset%)
              twondm)
      (if (> ndim 2)
          (setf (f2cl-lib:fref rulpts-%data% (8) ((1 wtleng)) rulpts-%offset%)
                  (coerce
                   (the f2cl-lib:integer4
                        (the f2cl-lib:integer4
                             (truncate (* 4 ndim (- ndim 1) (- ndim 2)) 3)))
                   'double-float)))
      (setf (f2cl-lib:fref rulpts-%data% (7) ((1 wtleng)) rulpts-%offset%)
              (coerce
               (the f2cl-lib:integer4
                    (f2cl-lib:int-mul 4 ndim (f2cl-lib:int-sub ndim 1)))
               'double-float))
      (setf (f2cl-lib:fref rulpts-%data% (6) ((1 wtleng)) rulpts-%offset%)
              (coerce
               (the f2cl-lib:integer4
                    (f2cl-lib:int-mul 2 ndim (f2cl-lib:int-sub ndim 1)))
               'double-float))
      (setf (f2cl-lib:fref rulpts-%data% (1) ((1 wtleng)) rulpts-%offset%)
              (coerce (the f2cl-lib:integer4 1) 'double-float))
      (setf lam0 (coerce 0.4707 'double-float))
      (setf lam1 (/ 4 (+ 15 (/ -5 lam0))))
      (setf ratio (/ (+ 1 (/ (- lam1) lam0)) 27))
      (setf lam2
              (/ (- 5 (* 7 lam1) (* 35 ratio))
                 (+ 7 (/ (* -35 lam1) 3) (/ (* -35 ratio) lam0))))
      (setf ratio (/ (* ratio (+ 1 (/ (- lam2) lam0))) 3))
      (setf lam3
              (/
               (- (+ (- 7 (* 9 (+ lam2 lam1))) (/ (* 63 lam2 lam1) 5))
                  (* 63 ratio))
               (+ 9
                  (/ (* -63 (+ lam2 lam1)) 5)
                  (* 21 lam2 lam1)
                  (/ (* -63 ratio) lam0))))
      (setf lamp (coerce 0.0625 'double-float))
      (setf (f2cl-lib:fref w-%data% (1 wtleng) ((1 5) (1 wtleng)) w-%offset%)
              (/ (/ 1 (expt (* 3 lam0) 4)) twondm))
      (if (> ndim 2)
          (setf (f2cl-lib:fref w-%data% (1 8) ((1 5) (1 wtleng)) w-%offset%)
                  (/ (+ 1 (/ -1 (* 3 lam0))) (expt (* 6 lam1) 3))))
      (setf (f2cl-lib:fref w-%data% (1 7) ((1 5) (1 wtleng)) w-%offset%)
              (/ (+ 1 (/ (* -7 (+ lam0 lam1)) 5) (/ (* 7 lam0 lam1) 3))
                 (* 84 lam1 lam2 (- lam2 lam0) (- lam2 lam1))))
      (setf (f2cl-lib:fref w-%data% (1 6) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/ (+ 1 (/ (* -7 (+ lam0 lam2)) 5) (/ (* 7 lam0 lam2) 3))
                  (* 84 lam1 lam1 (- lam1 lam0) (- lam1 lam2)))
               (/
                (*
                 (-
                  (f2cl-lib:fref w-%data% (1 7) ((1 5) (1 wtleng)) w-%offset%))
                 lam2)
                lam1)
               (* -2
                  (f2cl-lib:int-sub ndim 2)
                  (f2cl-lib:fref w-%data%
                                 (1 8)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (1 4) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+
                (- 1
                   (* 9
                      (+ (/ (+ lam0 lam1 lam2) 7)
                         (/ (- (+ (* lam0 lam1) (* lam0 lam2) (* lam1 lam2)))
                            5))))
                (* -3 lam0 lam1 lam2))
               (* 18 lam3 (- lam3 lam0) (- lam3 lam1) (- lam3 lam2))))
      (setf (f2cl-lib:fref w-%data% (1 3) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (+
                 (- 1
                    (* 9
                       (+ (/ (+ lam0 lam1 lam3) 7)
                          (/ (- (+ (* lam0 lam1) (* lam0 lam3) (* lam1 lam3)))
                             5))))
                 (* -3 lam0 lam1 lam3))
                (* 18 lam2 (- lam2 lam0) (- lam2 lam1) (- lam2 lam3)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (f2cl-lib:fref w-%data%
                                 (1 7)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (1 2) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (+
                 (- 1
                    (* 9
                       (+ (/ (+ lam0 lam2 lam3) 7)
                          (/ (- (+ (* lam0 lam2) (* lam0 lam3) (* lam2 lam3)))
                             5))))
                 (* -3 lam0 lam2 lam3))
                (* 18 lam1 (- lam1 lam0) (- lam1 lam2) (- lam1 lam3)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (+
                   (f2cl-lib:fref w-%data% (1 7) ((1 5) (1 wtleng)) w-%offset%)
                   (f2cl-lib:fref w-%data% (1 6) ((1 5) (1 wtleng)) w-%offset%)
                   (* (f2cl-lib:int-sub ndim 2)
                      (f2cl-lib:fref w-%data%
                                     (1 8)
                                     ((1 5) (1 wtleng))
                                     w-%offset%))))))
      (setf (f2cl-lib:fref w-%data% (2 wtleng) ((1 5) (1 wtleng)) w-%offset%)
              (/ (/ 1 (* 108 (expt lam0 4))) twondm))
      (if (> ndim 2)
          (setf (f2cl-lib:fref w-%data% (2 8) ((1 5) (1 wtleng)) w-%offset%)
                  (/
                   (+ 1
                      (* -27
                         twondm
                         (f2cl-lib:fref w-%data%
                                        (2 9)
                                        ((1 5) (1 wtleng))
                                        w-%offset%)
                         (expt lam0 3)))
                   (expt (* 6 lam1) 3))))
      (setf (f2cl-lib:fref w-%data% (2 7) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (/ (* -5 lam1) 3)
                  (* -15
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (2 wtleng)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     (expt lam0 2)
                     (- lam0 lam1)))
               (* 60 lam1 lam2 (- lam2 lam1))))
      (setf (f2cl-lib:fref w-%data% (2 6) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 1
                   (* 9
                      (+
                       (* 8
                          lam1
                          lam2
                          (f2cl-lib:fref w-%data%
                                         (2 7)
                                         ((1 5) (1 wtleng))
                                         w-%offset%))
                       (* twondm
                          (f2cl-lib:fref w-%data%
                                         (2 wtleng)
                                         ((1 5) (1 wtleng))
                                         w-%offset%)
                          (expt lam0 2)))))
                (* 36 lam1 lam1))
               (* -2
                  (f2cl-lib:fref w-%data% (2 8) ((1 5) (1 wtleng)) w-%offset%)
                  (f2cl-lib:int-sub ndim 2))))
      (setf (f2cl-lib:fref w-%data% (2 4) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (- 1
                  (* 7
                     (+ (/ (+ lam1 lam2) 5)
                        (/ (* (- lam1) lam2) 3)
                        (* twondm
                           (f2cl-lib:fref w-%data%
                                          (2 wtleng)
                                          ((1 5) (1 wtleng))
                                          w-%offset%)
                           lam0
                           (- lam0 lam1)
                           (- lam0 lam2)))))
               (* 14 lam3 (- lam3 lam1) (- lam3 lam2))))
      (setf (f2cl-lib:fref w-%data% (2 3) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 1
                   (* 7
                      (+ (/ (+ lam1 lam3) 5)
                         (/ (* (- lam1) lam3) 3)
                         (* twondm
                            (f2cl-lib:fref w-%data%
                                           (2 wtleng)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            lam0
                            (- lam0 lam1)
                            (- lam0 lam3)))))
                (* 14 lam2 (- lam2 lam1) (- lam2 lam3)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (f2cl-lib:fref w-%data%
                                 (2 7)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (2 2) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 1
                   (* 7
                      (+ (/ (+ lam2 lam3) 5)
                         (/ (* (- lam2) lam3) 3)
                         (* twondm
                            (f2cl-lib:fref w-%data%
                                           (2 wtleng)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            lam0
                            (- lam0 lam2)
                            (- lam0 lam3)))))
                (* 14 lam1 (- lam1 lam2) (- lam1 lam3)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (+
                   (f2cl-lib:fref w-%data% (2 7) ((1 5) (1 wtleng)) w-%offset%)
                   (f2cl-lib:fref w-%data% (2 6) ((1 5) (1 wtleng)) w-%offset%)
                   (* (f2cl-lib:int-sub ndim 2)
                      (f2cl-lib:fref w-%data%
                                     (2 8)
                                     ((1 5) (1 wtleng))
                                     w-%offset%))))))
      (setf (f2cl-lib:fref w-%data% (3 wtleng) ((1 5) (1 wtleng)) w-%offset%)
              (/ (/ 5 (* 324 (expt lam0 4))) twondm))
      (if (> ndim 2)
          (setf (f2cl-lib:fref w-%data% (3 8) ((1 5) (1 wtleng)) w-%offset%)
                  (/
                   (+ 1
                      (* -27
                         twondm
                         (f2cl-lib:fref w-%data%
                                        (3 9)
                                        ((1 5) (1 wtleng))
                                        w-%offset%)
                         (expt lam0 3)))
                   (expt (* 6 lam1) 3))))
      (setf (f2cl-lib:fref w-%data% (3 7) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 1
                  (/ (* -5 lam1) 3)
                  (* -15
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (3 wtleng)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     (expt lam0 2)
                     (- lam0 lam1)))
               (* 60 lam1 lam2 (- lam2 lam1))))
      (setf (f2cl-lib:fref w-%data% (3 6) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 1
                   (* 9
                      (+
                       (* 8
                          lam1
                          lam2
                          (f2cl-lib:fref w-%data%
                                         (3 7)
                                         ((1 5) (1 wtleng))
                                         w-%offset%))
                       (* twondm
                          (f2cl-lib:fref w-%data%
                                         (3 wtleng)
                                         ((1 5) (1 wtleng))
                                         w-%offset%)
                          (expt lam0 2)))))
                (* 36 lam1 lam1))
               (* -2
                  (f2cl-lib:fref w-%data% (3 8) ((1 5) (1 wtleng)) w-%offset%)
                  (f2cl-lib:int-sub ndim 2))))
      (setf (f2cl-lib:fref w-%data% (3 5) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (- 1
                  (* 7
                     (+ (/ (+ lam1 lam2) 5)
                        (/ (* (- lam1) lam2) 3)
                        (* twondm
                           (f2cl-lib:fref w-%data%
                                          (3 wtleng)
                                          ((1 5) (1 wtleng))
                                          w-%offset%)
                           lam0
                           (- lam0 lam1)
                           (- lam0 lam2)))))
               (* 14 lamp (- lamp lam1) (- lamp lam2))))
      (setf (f2cl-lib:fref w-%data% (3 3) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 1
                   (* 7
                      (+ (/ (+ lam1 lamp) 5)
                         (/ (* (- lam1) lamp) 3)
                         (* twondm
                            (f2cl-lib:fref w-%data%
                                           (3 wtleng)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            lam0
                            (- lam0 lam1)
                            (- lam0 lamp)))))
                (* 14 lam2 (- lam2 lam1) (- lam2 lamp)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (f2cl-lib:fref w-%data%
                                 (3 7)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (3 2) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 1
                   (* 7
                      (+ (/ (+ lam2 lamp) 5)
                         (/ (* (- lam2) lamp) 3)
                         (* twondm
                            (f2cl-lib:fref w-%data%
                                           (3 wtleng)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            lam0
                            (- lam0 lam2)
                            (- lam0 lamp)))))
                (* 14 lam1 (- lam1 lam2) (- lam1 lamp)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (+
                   (f2cl-lib:fref w-%data% (3 7) ((1 5) (1 wtleng)) w-%offset%)
                   (f2cl-lib:fref w-%data% (3 6) ((1 5) (1 wtleng)) w-%offset%)
                   (* (f2cl-lib:int-sub ndim 2)
                      (f2cl-lib:fref w-%data%
                                     (3 8)
                                     ((1 5) (1 wtleng))
                                     w-%offset%))))))
      (setf (f2cl-lib:fref w-%data% (4 wtleng) ((1 5) (1 wtleng)) w-%offset%)
              (/ (/ 2 (* 81 (expt lam0 4))) twondm))
      (if (> ndim 2)
          (setf (f2cl-lib:fref w-%data% (4 8) ((1 5) (1 wtleng)) w-%offset%)
                  (/
                   (+ 2
                      (* -27
                         twondm
                         (f2cl-lib:fref w-%data%
                                        (4 9)
                                        ((1 5) (1 wtleng))
                                        w-%offset%)
                         (expt lam0 3)))
                   (expt (* 6 lam1) 3))))
      (setf (f2cl-lib:fref w-%data% (4 7) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (+ 2
                  (/ (* -15 lam1) 9)
                  (* -15
                     twondm
                     (f2cl-lib:fref w-%data%
                                    (4 wtleng)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)
                     lam0
                     (- lam0 lam1)))
               (* 60 lam1 lam2 (- lam2 lam1))))
      (setf (f2cl-lib:fref w-%data% (4 6) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 1
                   (* 9
                      (+
                       (* 8
                          lam1
                          lam2
                          (f2cl-lib:fref w-%data%
                                         (4 7)
                                         ((1 5) (1 wtleng))
                                         w-%offset%))
                       (* twondm
                          (f2cl-lib:fref w-%data%
                                         (4 wtleng)
                                         ((1 5) (1 wtleng))
                                         w-%offset%)
                          (expt lam0 2)))))
                (* 36 lam1 lam1))
               (* -2
                  (f2cl-lib:fref w-%data% (4 8) ((1 5) (1 wtleng)) w-%offset%)
                  (f2cl-lib:int-sub ndim 2))))
      (setf (f2cl-lib:fref w-%data% (4 4) ((1 5) (1 wtleng)) w-%offset%)
              (/
               (- 2
                  (* 7
                     (+ (/ (+ lam1 lam2) 5)
                        (/ (* (- lam1) lam2) 3)
                        (* twondm
                           (f2cl-lib:fref w-%data%
                                          (4 wtleng)
                                          ((1 5) (1 wtleng))
                                          w-%offset%)
                           lam0
                           (- lam0 lam1)
                           (- lam0 lam2)))))
               (* 14 lam3 (- lam3 lam1) (- lam3 lam2))))
      (setf (f2cl-lib:fref w-%data% (4 3) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 2
                   (* 7
                      (+ (/ (+ lam1 lam3) 5)
                         (/ (* (- lam1) lam3) 3)
                         (* twondm
                            (f2cl-lib:fref w-%data%
                                           (4 wtleng)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            lam0
                            (- lam0 lam1)
                            (- lam0 lam3)))))
                (* 14 lam2 (- lam2 lam1) (- lam2 lam3)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (f2cl-lib:fref w-%data%
                                 (4 7)
                                 ((1 5) (1 wtleng))
                                 w-%offset%))))
      (setf (f2cl-lib:fref w-%data% (4 2) ((1 5) (1 wtleng)) w-%offset%)
              (+
               (/
                (- 2
                   (* 7
                      (+ (/ (+ lam2 lam3) 5)
                         (/ (* (- lam2) lam3) 3)
                         (* twondm
                            (f2cl-lib:fref w-%data%
                                           (4 wtleng)
                                           ((1 5) (1 wtleng))
                                           w-%offset%)
                            lam0
                            (- lam0 lam2)
                            (- lam0 lam3)))))
                (* 14 lam1 (- lam1 lam2) (- lam1 lam3)))
               (* -2
                  (f2cl-lib:int-sub ndim 1)
                  (+
                   (f2cl-lib:fref w-%data% (4 7) ((1 5) (1 wtleng)) w-%offset%)
                   (f2cl-lib:fref w-%data% (4 6) ((1 5) (1 wtleng)) w-%offset%)
                   (* (f2cl-lib:int-sub ndim 2)
                      (f2cl-lib:fref w-%data%
                                     (4 8)
                                     ((1 5) (1 wtleng))
                                     w-%offset%))))))
      (setf (f2cl-lib:fref w-%data% (5 2) ((1 5) (1 wtleng)) w-%offset%)
              (/ 1 (* 6 lam1)))
      (setf lam0 (f2cl-lib:fsqrt lam0))
      (setf lam1 (f2cl-lib:fsqrt lam1))
      (setf lam2 (f2cl-lib:fsqrt lam2))
      (setf lam3 (f2cl-lib:fsqrt lam3))
      (setf lamp (f2cl-lib:fsqrt lamp))
      (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                    ((> i ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref g-%data%
                               (i wtleng)
                               ((1 ndim) (1 wtleng))
                               g-%offset%)
                  lam0)
         label40))
      (cond
        ((> ndim 2)
         (setf (f2cl-lib:fref g-%data% (1 8) ((1 ndim) (1 wtleng)) g-%offset%)
                 lam1)
         (setf (f2cl-lib:fref g-%data% (2 8) ((1 ndim) (1 wtleng)) g-%offset%)
                 lam1)
         (setf (f2cl-lib:fref g-%data% (3 8) ((1 ndim) (1 wtleng)) g-%offset%)
                 lam1)))
      (setf (f2cl-lib:fref g-%data% (1 7) ((1 ndim) (1 wtleng)) g-%offset%)
              lam1)
      (setf (f2cl-lib:fref g-%data% (2 7) ((1 ndim) (1 wtleng)) g-%offset%)
              lam2)
      (setf (f2cl-lib:fref g-%data% (1 6) ((1 ndim) (1 wtleng)) g-%offset%)
              lam1)
      (setf (f2cl-lib:fref g-%data% (2 6) ((1 ndim) (1 wtleng)) g-%offset%)
              lam1)
      (setf (f2cl-lib:fref g-%data% (1 5) ((1 ndim) (1 wtleng)) g-%offset%)
              lamp)
      (setf (f2cl-lib:fref g-%data% (1 4) ((1 ndim) (1 wtleng)) g-%offset%)
              lam3)
      (setf (f2cl-lib:fref g-%data% (1 3) ((1 ndim) (1 wtleng)) g-%offset%)
              lam2)
      (setf (f2cl-lib:fref g-%data% (1 2) ((1 ndim) (1 wtleng)) g-%offset%)
              lam1)
      (setf (f2cl-lib:fref w-%data% (1 1) ((1 5) (1 wtleng)) w-%offset%)
              twondm)
      (f2cl-lib:fdo (j 2 (f2cl-lib:int-add j 1))
                    ((> j 5) nil)
        (tagbody
          (f2cl-lib:fdo (i 2 (f2cl-lib:int-add i 1))
                        ((> i wtleng) nil)
            (tagbody
              (setf (f2cl-lib:fref w-%data%
                                   (j i)
                                   ((1 5) (1 wtleng))
                                   w-%offset%)
                      (-
                       (f2cl-lib:fref w-%data%
                                      (j i)
                                      ((1 5) (1 wtleng))
                                      w-%offset%)
                       (f2cl-lib:fref w-%data%
                                      (1 i)
                                      ((1 5) (1 wtleng))
                                      w-%offset%)))
              (setf (f2cl-lib:fref w-%data%
                                   (j 1)
                                   ((1 5) (1 wtleng))
                                   w-%offset%)
                      (-
                       (f2cl-lib:fref w-%data%
                                      (j 1)
                                      ((1 5) (1 wtleng))
                                      w-%offset%)
                       (*
                        (f2cl-lib:fref rulpts-%data%
                                       (i)
                                       ((1 wtleng))
                                       rulpts-%offset%)
                        (f2cl-lib:fref w-%data%
                                       (j i)
                                       ((1 5) (1 wtleng))
                                       w-%offset%))))
             label50))
         label70))
      (f2cl-lib:fdo (i 2 (f2cl-lib:int-add i 1))
                    ((> i wtleng) nil)
        (tagbody
          (setf (f2cl-lib:fref w-%data% (1 i) ((1 5) (1 wtleng)) w-%offset%)
                  (* twondm
                     (f2cl-lib:fref w-%data%
                                    (1 i)
                                    ((1 5) (1 wtleng))
                                    w-%offset%)))
          (setf (f2cl-lib:fref w-%data% (1 1) ((1 5) (1 wtleng)) w-%offset%)
                  (-
                   (f2cl-lib:fref w-%data% (1 1) ((1 5) (1 wtleng)) w-%offset%)
                   (*
                    (f2cl-lib:fref rulpts-%data%
                                   (i)
                                   ((1 wtleng))
                                   rulpts-%offset%)
                    (f2cl-lib:fref w-%data%
                                   (1 i)
                                   ((1 5) (1 wtleng))
                                   w-%offset%))))
         label80))
      (setf (f2cl-lib:fref errcof-%data% (1) ((1 6)) errcof-%offset%)
              (coerce (the f2cl-lib:integer4 5) 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (2) ((1 6)) errcof-%offset%)
              (coerce (the f2cl-lib:integer4 5) 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (3) ((1 6)) errcof-%offset%)
              (coerce 1.0 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (4) ((1 6)) errcof-%offset%)
              (coerce (the f2cl-lib:integer4 5) 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (5) ((1 6)) errcof-%offset%)
              (coerce 0.5 'double-float))
      (setf (f2cl-lib:fref errcof-%data% (6) ((1 6)) errcof-%offset%)
              (coerce 0.25 'double-float))
     end_label
      (return (values nil nil nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::d09hre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*)))
           :return-values '(nil nil nil nil nil nil)
           :calls 'nil)))

