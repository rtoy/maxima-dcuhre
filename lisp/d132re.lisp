;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(let ((dim2g (make-array 16 :element-type 'double-float))
      (dim2w (make-array 70 :element-type 'double-float)))
  (declare (type (array double-float (16)) dim2g)
           (type (array double-float (70)) dim2w))
  (f2cl-lib:data-implied-do (((dim2g (i))) (i 1 16))
                            (((1 16)))
                            ((double-float))
                            (0.2517129343453109d0 0.7013933644534266d0
                             0.9590960631619962d0 0.9956010478552128d0 0.5d0
                             0.1594544658297559d0 0.3808991135940188d0
                             0.6582769255267192d0 0.8761473165029315d0
                             0.998243184053198d0 0.9790222658168461d0
                             0.6492284325645389d0 0.8727421201131239d0
                             0.3582614645881228d0 0.5666666666666667d0
                             0.2077777777777778d0))
  (f2cl-lib:data-implied-do (((dim2w (i 1))) (i 1 14))
                            (((1 14) (1 5)))
                            ((double-float))
                            (0.0337969236013446d0 0.09508589607597762d0
                             0.1176006468056962d0 0.0265777458632695d0
                             0.0170144177020064d0 0.0d0 0.0162659309863741d0
                             0.1344892658526199d0 0.1328032165460149d0
                             0.0563747476999187d0 0.0039082790813105d0
                             0.0301279877743215d0 0.1030873234689166d0
                             0.0625d0))
  (f2cl-lib:data-implied-do (((dim2w (i 2))) (i 1 14))
                            (((1 14) (1 5)))
                            ((double-float))
                            (0.3213775489050763d0 -0.1767341636743844d0
                             0.07347600537466072d0 -0.03638022004364754d0
                             0.02125297922098712d0 0.1460984204026913d0
                             0.01747613286152099d0 0.1444954045641582d0
                             1.307687976001325d-4 5.380992313941161d-4
                             1.042259576889814d-4 -0.001401152865045733d0
                             0.008041788181514763d0 -0.1420416552759383d0))
  (f2cl-lib:data-implied-do (((dim2w (i 3))) (i 1 14))
                            (((1 14) (1 5)))
                            ((double-float))
                            (0.3372900883288987d0 -0.1644903060344491d0
                             0.07707849911634622d0 -0.0380447835850631d0
                             0.02223559940380806d0 0.1480693879765931d0
                             4.467143702185814d-6 0.150894476707413d0
                             3.647200107516215d-5 5.77719899901388d-4
                             1.041757313688177d-4 -0.001452822267047819d0
                             0.008338339968783704d0 -0.147279632923196d0))
  (f2cl-lib:data-implied-do (((dim2w (i 4))) (i 1 14))
                            (((1 14) (1 5)))
                            ((double-float))
                            (-0.8264123822525677d0 0.306583861409436d0
                             0.002389292538329435d0 -0.1343024157997222d0
                             0.088333668405339d0 0.0d0 9.786283074168292d-4
                             -0.1319227889147519d0 0.00799001220015063d0
                             0.003391747079760626d0 0.002294915718283264d0
                             -0.01358584986119197d0 0.04025866859057809d0
                             0.003760268580063992d0))
  (f2cl-lib:data-implied-do (((dim2w (i 5))) (i 1 14))
                            (((1 14) (1 5)))
                            ((double-float))
                            (0.6539094339575232d0 -0.2041614154424632d0
                             -0.174698151579499d0 0.03937939671417803d0
                             0.006974520545933992d0 0.0d0
                             0.006667702171778258d0 0.05512960621544304d0
                             0.05443846381278607d0 0.02310903863953934d0
                             0.01506937747477189d0 -0.0605702164890189d0
                             0.04225737654686337d0 0.02561989142123099d0))
  (defun d132re (wtleng w g errcof rulpts)
    (declare (type (array double-float (*)) errcof)
             (type (array double-float (*)) rulpts g w)
             (type (f2cl-lib:integer4) wtleng))
    (f2cl-lib:with-multi-array-data
        ((w double-float w-%data% w-%offset%)
         (g double-float g-%data% g-%offset%)
         (rulpts double-float rulpts-%data% rulpts-%offset%)
         (errcof double-float errcof-%data% errcof-%offset%))
      (prog ((i 0) (j 0))
        (declare (type (f2cl-lib:integer4) j i))
        (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                      ((> i 14) nil)
          (tagbody
            (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                          ((> j 5) nil)
              (tagbody
                (setf (f2cl-lib:fref w-%data%
                                     (j i)
                                     ((1 5) (1 wtleng))
                                     w-%offset%)
                        (f2cl-lib:fref dim2w (i j) ((1 14) (1 5))))
               label10))))
       label10
        (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                      ((> i 2) nil)
          (tagbody
            (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                          ((> j 14) nil)
              (tagbody
                (setf (f2cl-lib:fref g-%data%
                                     (i j)
                                     ((1 2) (1 wtleng))
                                     g-%offset%)
                        (coerce (the f2cl-lib:integer4 0) 'double-float))
               label20))))
       label20
        (setf (f2cl-lib:fref g-%data% (1 2) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (1) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (1 3) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (2) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (1 4) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (3) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (1 5) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (4) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (1 6) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (5) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (1 7) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (6) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (2 7) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 7) ((1 2) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 8) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (7) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (2 8) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 8) ((1 2) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 9) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (8) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (2 9) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 9) ((1 2) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 10) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (9) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (2 10) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 10) ((1 2) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 11) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (10) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (2 11) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref g-%data% (1 11) ((1 2) (1 wtleng)) g-%offset%))
        (setf (f2cl-lib:fref g-%data% (1 12) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (11) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (2 12) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (12) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (1 13) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (13) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (2 13) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (14) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (1 14) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (15) ((1 16))))
        (setf (f2cl-lib:fref g-%data% (2 14) ((1 2) (1 wtleng)) g-%offset%)
                (f2cl-lib:fref dim2g (16) ((1 16))))
        (setf (f2cl-lib:fref rulpts-%data% (1) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 1) 'double-float))
        (f2cl-lib:fdo (i 2 (f2cl-lib:int-add i 1))
                      ((> i 11) nil)
          (tagbody
            (setf (f2cl-lib:fref rulpts-%data%
                                 (i)
                                 ((1 wtleng))
                                 rulpts-%offset%)
                    (coerce (the f2cl-lib:integer4 4) 'double-float))
           label30))
        (setf (f2cl-lib:fref rulpts-%data% (12) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 8) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (13) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 8) 'double-float))
        (setf (f2cl-lib:fref rulpts-%data% (14) ((1 wtleng)) rulpts-%offset%)
                (coerce (the f2cl-lib:integer4 8) 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (1) ((1 6)) errcof-%offset%)
                (coerce (the f2cl-lib:integer4 10) 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (2) ((1 6)) errcof-%offset%)
                (coerce (the f2cl-lib:integer4 10) 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (3) ((1 6)) errcof-%offset%)
                (coerce 1.0 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (4) ((1 6)) errcof-%offset%)
                (coerce 5.0 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (5) ((1 6)) errcof-%offset%)
                (coerce 0.5 'double-float))
        (setf (f2cl-lib:fref errcof-%data% (6) ((1 6)) errcof-%offset%)
                (coerce 0.25 'double-float))
        (go end_label)
       end_label
        (return (values nil nil nil nil nil))))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::d132re
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)))
           :return-values '(nil nil nil nil nil)
           :calls 'nil)))

