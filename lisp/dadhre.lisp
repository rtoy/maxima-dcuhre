;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun dadhre
       (ndim numfun mdiv a b minsub maxsub funsub epsabs epsrel key restar num
        lenw wtleng result abserr neval nsub ifail values$ errors centrs hwidts
        greate dir oldres work g w rulpts center hwidth x scales norms)
  (declare (type (double-float) epsrel epsabs)
           (type (array double-float (*)) norms scales x hwidth center rulpts w
                                          g work oldres dir greate hwidts
                                          centrs errors values$ abserr result b
                                          a)
           (type (f2cl-lib:integer4) ifail nsub neval wtleng lenw num restar
                                     key maxsub minsub mdiv numfun ndim))
  (f2cl-lib:with-multi-array-data
      ((a double-float a-%data% a-%offset%)
       (b double-float b-%data% b-%offset%)
       (result double-float result-%data% result-%offset%)
       (abserr double-float abserr-%data% abserr-%offset%)
       (values$ double-float values$-%data% values$-%offset%)
       (errors double-float errors-%data% errors-%offset%)
       (centrs double-float centrs-%data% centrs-%offset%)
       (hwidts double-float hwidts-%data% hwidts-%offset%)
       (greate double-float greate-%data% greate-%offset%)
       (dir double-float dir-%data% dir-%offset%)
       (oldres double-float oldres-%data% oldres-%offset%)
       (work double-float work-%data% work-%offset%)
       (g double-float g-%data% g-%offset%)
       (w double-float w-%data% w-%offset%)
       (rulpts double-float rulpts-%data% rulpts-%offset%)
       (center double-float center-%data% center-%offset%)
       (hwidth double-float hwidth-%data% hwidth-%offset%)
       (x double-float x-%data% x-%offset%)
       (scales double-float scales-%data% scales-%offset%)
       (norms double-float norms-%data% norms-%offset%))
    (prog ((oldcen 0.0d0) (est1 0.0d0) (est2 0.0d0)
           (errcof (make-array 6 :element-type 'double-float)) (ndiv 0)
           (pointr 0) (direct 0) (f2cl-lib:index 0) (l1 0) (intsgn 0)
           (sbrgns 0) (i 0) (j 0) (k 0))
      (declare (type (f2cl-lib:integer4) k j i sbrgns intsgn l1 f2cl-lib:index
                                         direct pointr ndiv)
               (type (array double-float (6)) errcof)
               (type (double-float) est2 est1 oldcen))
      (setf intsgn 1)
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j ndim) nil)
        (tagbody
          (cond
            ((< (f2cl-lib:fref b (j) ((1 ndim)))
                (f2cl-lib:fref a (j) ((1 ndim))))
             (setf intsgn (f2cl-lib:int-sub intsgn))))
         label10))
      (dinhre ndim key wtleng w g errcof rulpts scales norms)
      (cond
        ((= restar 1)
         (setf sbrgns nsub)
         (go label110)))
      (setf sbrgns 1)
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref centrs-%data%
                               (j 1)
                               ((1 ndim) (1 maxsub))
                               centrs-%offset%)
                  (/
                   (+ (f2cl-lib:fref a-%data% (j) ((1 ndim)) a-%offset%)
                      (f2cl-lib:fref b-%data% (j) ((1 ndim)) b-%offset%))
                   2))
          (setf (f2cl-lib:fref hwidts-%data%
                               (j 1)
                               ((1 ndim) (1 maxsub))
                               hwidts-%offset%)
                  (/
                   (abs
                    (- (f2cl-lib:fref b-%data% (j) ((1 ndim)) b-%offset%)
                       (f2cl-lib:fref a-%data% (j) ((1 ndim)) a-%offset%)))
                   2))
         label15))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref result-%data% (j) ((1 numfun)) result-%offset%)
                  (coerce (the f2cl-lib:integer4 0) 'double-float))
          (setf (f2cl-lib:fref abserr-%data% (j) ((1 numfun)) abserr-%offset%)
                  (coerce (the f2cl-lib:integer4 0) 'double-float))
         label20))
      (setf neval 0)
      (multiple-value-bind
            (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8 var-9 var-10
             var-11 var-12 var-13 var-14 var-15)
          (drlhre ndim
           (f2cl-lib:array-slice centrs-%data%
                                 double-float
                                 (1 1)
                                 ((1 ndim) (1 maxsub))
                                 centrs-%offset%)
           (f2cl-lib:array-slice hwidts-%data%
                                 double-float
                                 (1 1)
                                 ((1 ndim) (1 maxsub))
                                 hwidts-%offset%)
           wtleng g w errcof numfun funsub scales norms x work
           (f2cl-lib:array-slice values$-%data%
                                 double-float
                                 (1 1)
                                 ((1 numfun) (1 maxsub))
                                 values$-%offset%)
           (f2cl-lib:array-slice errors-%data%
                                 double-float
                                 (1 1)
                                 ((1 numfun) (1 maxsub))
                                 errors-%offset%)
           (f2cl-lib:fref dir-%data% (1) ((1 maxsub)) dir-%offset%))
        (declare (ignore var-1 var-2 var-3 var-4 var-5 var-6 var-8 var-9 var-10
                         var-11 var-12 var-13 var-14))
        (setf ndim var-0)
        (setf numfun var-7)
        (setf (f2cl-lib:fref dir-%data% (1) ((1 maxsub)) dir-%offset%) var-15))
      (setf neval (f2cl-lib:int-add neval num))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref result-%data% (j) ((1 numfun)) result-%offset%)
                  (+
                   (f2cl-lib:fref result-%data%
                                  (j)
                                  ((1 numfun))
                                  result-%offset%)
                   (f2cl-lib:fref values$-%data%
                                  (j 1)
                                  ((1 numfun) (1 maxsub))
                                  values$-%offset%)))
         label55))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref abserr-%data% (j) ((1 numfun)) abserr-%offset%)
                  (+
                   (f2cl-lib:fref abserr-%data%
                                  (j)
                                  ((1 numfun))
                                  abserr-%offset%)
                   (f2cl-lib:fref errors-%data%
                                  (j 1)
                                  ((1 numfun) (1 maxsub))
                                  errors-%offset%)))
         label65))
      (setf f2cl-lib:index 1)
      (dtrhre 2 ndim numfun f2cl-lib:index values$ errors centrs hwidts greate
       (f2cl-lib:array-slice work-%data%
                             double-float
                             (1)
                             ((1 lenw))
                             work-%offset%)
       (f2cl-lib:array-slice work-%data%
                             double-float
                             ((+ numfun 1))
                             ((1 lenw))
                             work-%offset%)
       center hwidth dir)
     label110
      (cond
        ((<= (f2cl-lib:int-add sbrgns 1) maxsub)
         (tagbody
           (cond
             ((> mdiv 1)
              (setf ndiv (f2cl-lib:int-sub maxsub sbrgns))
              (setf ndiv
                      (min (the f2cl-lib:integer4 ndiv)
                           (the f2cl-lib:integer4 mdiv)
                           (the f2cl-lib:integer4 sbrgns))))
             (t
              (setf ndiv 1)))
           (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                         ((> i ndiv) nil)
             (tagbody
               (setf pointr
                       (f2cl-lib:int-sub (f2cl-lib:int-add sbrgns ndiv 1) i))
               (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                             ((> j numfun) nil)
                 (tagbody
                   (setf (f2cl-lib:fref result-%data%
                                        (j)
                                        ((1 numfun))
                                        result-%offset%)
                           (-
                            (f2cl-lib:fref result-%data%
                                           (j)
                                           ((1 numfun))
                                           result-%offset%)
                            (f2cl-lib:fref values$-%data%
                                           (j 1)
                                           ((1 numfun) (1 maxsub))
                                           values$-%offset%)))
                   (setf (f2cl-lib:fref abserr-%data%
                                        (j)
                                        ((1 numfun))
                                        abserr-%offset%)
                           (-
                            (f2cl-lib:fref abserr-%data%
                                           (j)
                                           ((1 numfun))
                                           abserr-%offset%)
                            (f2cl-lib:fref errors-%data%
                                           (j 1)
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)))
                  label115))
               (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                             ((> j ndim) nil)
                 (tagbody
                   (setf (f2cl-lib:fref centrs-%data%
                                        (j pointr)
                                        ((1 ndim) (1 maxsub))
                                        centrs-%offset%)
                           (f2cl-lib:fref centrs-%data%
                                          (j 1)
                                          ((1 ndim) (1 maxsub))
                                          centrs-%offset%))
                   (setf (f2cl-lib:fref hwidts-%data%
                                        (j pointr)
                                        ((1 ndim) (1 maxsub))
                                        hwidts-%offset%)
                           (f2cl-lib:fref hwidts-%data%
                                          (j 1)
                                          ((1 ndim) (1 maxsub))
                                          hwidts-%offset%))
                  label120))
               (setf direct
                       (f2cl-lib:int
                        (f2cl-lib:fref dir-%data%
                                       (1)
                                       ((1 maxsub))
                                       dir-%offset%)))
               (setf (f2cl-lib:fref dir-%data%
                                    (pointr)
                                    ((1 maxsub))
                                    dir-%offset%)
                       (coerce (the f2cl-lib:integer4 direct) 'double-float))
               (setf (f2cl-lib:fref hwidts-%data%
                                    (direct pointr)
                                    ((1 ndim) (1 maxsub))
                                    hwidts-%offset%)
                       (/
                        (f2cl-lib:fref hwidts-%data%
                                       (direct 1)
                                       ((1 ndim) (1 maxsub))
                                       hwidts-%offset%)
                        2))
               (setf oldcen
                       (f2cl-lib:fref centrs-%data%
                                      (direct 1)
                                      ((1 ndim) (1 maxsub))
                                      centrs-%offset%))
               (setf (f2cl-lib:fref centrs-%data%
                                    (direct pointr)
                                    ((1 ndim) (1 maxsub))
                                    centrs-%offset%)
                       (- oldcen
                          (f2cl-lib:fref hwidts-%data%
                                         (direct pointr)
                                         ((1 ndim) (1 maxsub))
                                         hwidts-%offset%)))
               (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                             ((> j numfun) nil)
                 (tagbody
                   (setf (f2cl-lib:fref oldres-%data%
                                        (j
                                         (f2cl-lib:int-add
                                          (f2cl-lib:int-sub ndiv i)
                                          1))
                                        ((1 numfun) (1 mdiv))
                                        oldres-%offset%)
                           (f2cl-lib:fref values$-%data%
                                          (j 1)
                                          ((1 numfun) (1 maxsub))
                                          values$-%offset%))
                  label125))
               (multiple-value-bind
                     (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8
                      var-9 var-10 var-11 var-12 var-13)
                   (dtrhre 1 ndim numfun sbrgns values$ errors centrs hwidts
                    greate
                    (f2cl-lib:array-slice work-%data%
                                          double-float
                                          (1)
                                          ((1 lenw))
                                          work-%offset%)
                    (f2cl-lib:array-slice work-%data%
                                          double-float
                                          ((+ numfun 1))
                                          ((1 lenw))
                                          work-%offset%)
                    center hwidth dir)
                 (declare (ignore var-0 var-1 var-2 var-4 var-5 var-6 var-7
                                  var-8 var-9 var-10 var-11 var-12 var-13))
                 (setf sbrgns var-3))
               (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                             ((> j ndim) nil)
                 (tagbody
                   (setf (f2cl-lib:fref centrs-%data%
                                        (j (f2cl-lib:int-sub pointr 1))
                                        ((1 ndim) (1 maxsub))
                                        centrs-%offset%)
                           (f2cl-lib:fref centrs-%data%
                                          (j pointr)
                                          ((1 ndim) (1 maxsub))
                                          centrs-%offset%))
                   (setf (f2cl-lib:fref hwidts-%data%
                                        (j (f2cl-lib:int-sub pointr 1))
                                        ((1 ndim) (1 maxsub))
                                        hwidts-%offset%)
                           (f2cl-lib:fref hwidts-%data%
                                          (j pointr)
                                          ((1 ndim) (1 maxsub))
                                          hwidts-%offset%))
                  label130))
               (setf (f2cl-lib:fref centrs-%data%
                                    (direct (f2cl-lib:int-sub pointr 1))
                                    ((1 ndim) (1 maxsub))
                                    centrs-%offset%)
                       (+ oldcen
                          (f2cl-lib:fref hwidts-%data%
                                         (direct pointr)
                                         ((1 ndim) (1 maxsub))
                                         hwidts-%offset%)))
               (setf (f2cl-lib:fref hwidts-%data%
                                    (direct (f2cl-lib:int-sub pointr 1))
                                    ((1 ndim) (1 maxsub))
                                    hwidts-%offset%)
                       (f2cl-lib:fref hwidts-%data%
                                      (direct pointr)
                                      ((1 ndim) (1 maxsub))
                                      hwidts-%offset%))
               (setf (f2cl-lib:fref dir-%data%
                                    ((f2cl-lib:int-sub pointr 1))
                                    ((1 maxsub))
                                    dir-%offset%)
                       (coerce (the f2cl-lib:integer4 direct) 'double-float))
              label150))
           (f2cl-lib:fdo (i 2 (f2cl-lib:int-add i 1))
                         ((> i (f2cl-lib:int-mul 2 ndiv)) nil)
             (tagbody
               (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                             ((> j ndim) nil)
                 (tagbody
                   (f2cl-lib:fdo (k 1 (f2cl-lib:int-add k 1))
                                 ((> k wtleng) nil)
                     (tagbody
                       (setf (f2cl-lib:fref g-%data%
                                            (j k i)
                                            ((1 ndim) (1 wtleng)
                                             (1 (f2cl-lib:int-mul 2 mdiv)))
                                            g-%offset%)
                               (f2cl-lib:fref g-%data%
                                              (j k 1)
                                              ((1 ndim) (1 wtleng)
                                               (1 (f2cl-lib:int-mul 2 mdiv)))
                                              g-%offset%))
                      label190))))))
          label190
           (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                         ((> i (f2cl-lib:int-mul 2 ndiv)) nil)
             (tagbody
               (setf f2cl-lib:index (f2cl-lib:int-add sbrgns i))
               (setf l1
                       (f2cl-lib:int-add 1
                                         (f2cl-lib:int-mul
                                          (f2cl-lib:int-sub i 1)
                                          8
                                          numfun)))
               (multiple-value-bind
                     (var-0 var-1 var-2 var-3 var-4 var-5 var-6 var-7 var-8
                      var-9 var-10 var-11 var-12 var-13 var-14 var-15)
                   (drlhre ndim
                    (f2cl-lib:array-slice centrs-%data%
                                          double-float
                                          (1 f2cl-lib:index)
                                          ((1 ndim) (1 maxsub))
                                          centrs-%offset%)
                    (f2cl-lib:array-slice hwidts-%data%
                                          double-float
                                          (1 f2cl-lib:index)
                                          ((1 ndim) (1 maxsub))
                                          hwidts-%offset%)
                    wtleng
                    (f2cl-lib:array-slice g-%data%
                                          double-float
                                          (1 1 i)
                                          ((1 ndim) (1 wtleng)
                                           (1 (f2cl-lib:int-mul 2 mdiv)))
                                          g-%offset%)
                    w errcof numfun funsub scales norms
                    (f2cl-lib:array-slice x-%data%
                                          double-float
                                          (1 i)
                                          ((1 ndim)
                                           (1 (f2cl-lib:int-mul 2 mdiv)))
                                          x-%offset%)
                    (f2cl-lib:array-slice work-%data%
                                          double-float
                                          (l1)
                                          ((1 lenw))
                                          work-%offset%)
                    (f2cl-lib:array-slice values$-%data%
                                          double-float
                                          (1 f2cl-lib:index)
                                          ((1 numfun) (1 maxsub))
                                          values$-%offset%)
                    (f2cl-lib:array-slice errors-%data%
                                          double-float
                                          (1 f2cl-lib:index)
                                          ((1 numfun) (1 maxsub))
                                          errors-%offset%)
                    (f2cl-lib:fref dir-%data%
                                   (f2cl-lib:index)
                                   ((1 maxsub))
                                   dir-%offset%))
                 (declare (ignore var-1 var-2 var-3 var-4 var-5 var-6 var-8
                                  var-9 var-10 var-11 var-12 var-13 var-14))
                 (setf ndim var-0)
                 (setf numfun var-7)
                 (setf (f2cl-lib:fref dir-%data%
                                      (f2cl-lib:index)
                                      ((1 maxsub))
                                      dir-%offset%)
                         var-15))
              label200))
           (setf neval (f2cl-lib:int-add neval (f2cl-lib:int-mul 2 ndiv num)))
           (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                         ((> i (f2cl-lib:int-mul 2 ndiv)) nil)
             (tagbody
               (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                             ((> j numfun) nil)
                 (tagbody
                   (setf (f2cl-lib:fref result-%data%
                                        (j)
                                        ((1 numfun))
                                        result-%offset%)
                           (+
                            (f2cl-lib:fref result-%data%
                                           (j)
                                           ((1 numfun))
                                           result-%offset%)
                            (f2cl-lib:fref values$-%data%
                                           (j (f2cl-lib:int-add sbrgns i))
                                           ((1 numfun) (1 maxsub))
                                           values$-%offset%)))
                  label210))
              label220))
           (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                         ((> i ndiv) nil)
             (tagbody
               (setf (f2cl-lib:fref greate-%data%
                                    ((f2cl-lib:int-sub
                                      (f2cl-lib:int-add sbrgns
                                                        (f2cl-lib:int-mul 2 i))
                                      1))
                                    ((1 maxsub))
                                    greate-%offset%)
                       (coerce (the f2cl-lib:integer4 0) 'double-float))
               (setf (f2cl-lib:fref greate-%data%
                                    ((f2cl-lib:int-add sbrgns
                                                       (f2cl-lib:int-mul 2 i)))
                                    ((1 maxsub))
                                    greate-%offset%)
                       (coerce (the f2cl-lib:integer4 0) 'double-float))
               (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                             ((> j numfun) nil)
                 (tagbody
                   (setf est1
                           (abs
                            (-
                             (f2cl-lib:fref oldres-%data%
                                            (j i)
                                            ((1 numfun) (1 mdiv))
                                            oldres-%offset%)
                             (+
                              (f2cl-lib:fref values$-%data%
                                             (j
                                              (f2cl-lib:int-sub
                                               (f2cl-lib:int-add sbrgns
                                                                 (f2cl-lib:int-mul
                                                                  2
                                                                  i))
                                               1))
                                             ((1 numfun) (1 maxsub))
                                             values$-%offset%)
                              (f2cl-lib:fref values$-%data%
                                             (j
                                              (f2cl-lib:int-add sbrgns
                                                                (f2cl-lib:int-mul
                                                                 2
                                                                 i)))
                                             ((1 numfun) (1 maxsub))
                                             values$-%offset%)))))
                   (setf est2
                           (+
                            (f2cl-lib:fref errors-%data%
                                           (j
                                            (f2cl-lib:int-sub
                                             (f2cl-lib:int-add sbrgns
                                                               (f2cl-lib:int-mul
                                                                2
                                                                i))
                                             1))
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)
                            (f2cl-lib:fref errors-%data%
                                           (j
                                            (f2cl-lib:int-add sbrgns
                                                              (f2cl-lib:int-mul
                                                               2
                                                               i)))
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)))
                   (cond
                     ((> est2 0)
                      (setf (f2cl-lib:fref errors-%data%
                                           (j
                                            (f2cl-lib:int-sub
                                             (f2cl-lib:int-add sbrgns
                                                               (f2cl-lib:int-mul
                                                                2
                                                                i))
                                             1))
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)
                              (*
                               (f2cl-lib:fref errors-%data%
                                              (j
                                               (f2cl-lib:int-sub
                                                (f2cl-lib:int-add sbrgns
                                                                  (f2cl-lib:int-mul
                                                                   2
                                                                   i))
                                                1))
                                              ((1 numfun) (1 maxsub))
                                              errors-%offset%)
                               (+ 1
                                  (/
                                   (* (f2cl-lib:fref errcof (5) ((1 6))) est1)
                                   est2))))
                      (setf (f2cl-lib:fref errors-%data%
                                           (j
                                            (f2cl-lib:int-add sbrgns
                                                              (f2cl-lib:int-mul
                                                               2
                                                               i)))
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)
                              (*
                               (f2cl-lib:fref errors-%data%
                                              (j
                                               (f2cl-lib:int-add sbrgns
                                                                 (f2cl-lib:int-mul
                                                                  2
                                                                  i)))
                                              ((1 numfun) (1 maxsub))
                                              errors-%offset%)
                               (+ 1
                                  (/
                                   (* (f2cl-lib:fref errcof (5) ((1 6))) est1)
                                   est2))))))
                   (setf (f2cl-lib:fref errors-%data%
                                        (j
                                         (f2cl-lib:int-sub
                                          (f2cl-lib:int-add sbrgns
                                                            (f2cl-lib:int-mul 2
                                                                              i))
                                          1))
                                        ((1 numfun) (1 maxsub))
                                        errors-%offset%)
                           (+
                            (f2cl-lib:fref errors-%data%
                                           (j
                                            (f2cl-lib:int-sub
                                             (f2cl-lib:int-add sbrgns
                                                               (f2cl-lib:int-mul
                                                                2
                                                                i))
                                             1))
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)
                            (* (f2cl-lib:fref errcof (6) ((1 6))) est1)))
                   (setf (f2cl-lib:fref errors-%data%
                                        (j
                                         (f2cl-lib:int-add sbrgns
                                                           (f2cl-lib:int-mul 2
                                                                             i)))
                                        ((1 numfun) (1 maxsub))
                                        errors-%offset%)
                           (+
                            (f2cl-lib:fref errors-%data%
                                           (j
                                            (f2cl-lib:int-add sbrgns
                                                              (f2cl-lib:int-mul
                                                               2
                                                               i)))
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)
                            (* (f2cl-lib:fref errcof (6) ((1 6))) est1)))
                   (cond
                     ((>
                       (f2cl-lib:fref errors
                                      (j
                                       (f2cl-lib:int-add sbrgns
                                                         (f2cl-lib:int-mul 2 i)
                                                         (f2cl-lib:int-sub 1)))
                                      ((1 numfun) (1 maxsub)))
                       (f2cl-lib:fref greate
                                      ((f2cl-lib:int-add sbrgns
                                                         (f2cl-lib:int-mul 2 i)
                                                         (f2cl-lib:int-sub 1)))
                                      ((1 maxsub))))
                      (setf (f2cl-lib:fref greate-%data%
                                           ((f2cl-lib:int-sub
                                             (f2cl-lib:int-add sbrgns
                                                               (f2cl-lib:int-mul
                                                                2
                                                                i))
                                             1))
                                           ((1 maxsub))
                                           greate-%offset%)
                              (f2cl-lib:fref errors-%data%
                                             (j
                                              (f2cl-lib:int-sub
                                               (f2cl-lib:int-add sbrgns
                                                                 (f2cl-lib:int-mul
                                                                  2
                                                                  i))
                                               1))
                                             ((1 numfun) (1 maxsub))
                                             errors-%offset%))))
                   (cond
                     ((>
                       (f2cl-lib:fref errors
                                      (j
                                       (f2cl-lib:int-add sbrgns
                                                         (f2cl-lib:int-mul 2
                                                                           i)))
                                      ((1 numfun) (1 maxsub)))
                       (f2cl-lib:fref greate
                                      ((f2cl-lib:int-add sbrgns
                                                         (f2cl-lib:int-mul 2
                                                                           i)))
                                      ((1 maxsub))))
                      (setf (f2cl-lib:fref greate-%data%
                                           ((f2cl-lib:int-add sbrgns
                                                              (f2cl-lib:int-mul
                                                               2
                                                               i)))
                                           ((1 maxsub))
                                           greate-%offset%)
                              (f2cl-lib:fref errors-%data%
                                             (j
                                              (f2cl-lib:int-add sbrgns
                                                                (f2cl-lib:int-mul
                                                                 2
                                                                 i)))
                                             ((1 numfun) (1 maxsub))
                                             errors-%offset%))))
                   (setf (f2cl-lib:fref abserr-%data%
                                        (j)
                                        ((1 numfun))
                                        abserr-%offset%)
                           (+
                            (f2cl-lib:fref abserr-%data%
                                           (j)
                                           ((1 numfun))
                                           abserr-%offset%)
                            (f2cl-lib:fref errors-%data%
                                           (j
                                            (f2cl-lib:int-sub
                                             (f2cl-lib:int-add sbrgns
                                                               (f2cl-lib:int-mul
                                                                2
                                                                i))
                                             1))
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)
                            (f2cl-lib:fref errors-%data%
                                           (j
                                            (f2cl-lib:int-add sbrgns
                                                              (f2cl-lib:int-mul
                                                               2
                                                               i)))
                                           ((1 numfun) (1 maxsub))
                                           errors-%offset%)))
                  label230))
              label240))
           (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                         ((> i (f2cl-lib:int-mul 2 ndiv)) nil)
             (tagbody
               (setf f2cl-lib:index (f2cl-lib:int-add sbrgns i))
               (dtrhre 2 ndim numfun f2cl-lib:index values$ errors centrs
                hwidts greate
                (f2cl-lib:array-slice work-%data%
                                      double-float
                                      (1)
                                      ((1 lenw))
                                      work-%offset%)
                (f2cl-lib:array-slice work-%data%
                                      double-float
                                      ((+ numfun 1))
                                      ((1 lenw))
                                      work-%offset%)
                center hwidth dir)
              label250))
           (setf sbrgns (f2cl-lib:int-add sbrgns (f2cl-lib:int-mul 2 ndiv)))
           (cond
             ((< sbrgns minsub)
              (go label110)))
           (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                         ((> j numfun) nil)
             (tagbody
               (cond
                 ((and
                   (> (f2cl-lib:fref abserr (j) ((1 numfun)))
                      (* epsrel (abs (f2cl-lib:fref result (j) ((1 numfun))))))
                   (> (f2cl-lib:fref abserr (j) ((1 numfun))) epsabs))
                  (go label110)))
              label255))
           (setf ifail 0)
           (go label499)))
        (t
         (setf ifail 1)))
     label499
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref result-%data% (j) ((1 numfun)) result-%offset%)
                  (coerce (the f2cl-lib:integer4 0) 'double-float))
          (setf (f2cl-lib:fref abserr-%data% (j) ((1 numfun)) abserr-%offset%)
                  (coerce (the f2cl-lib:integer4 0) 'double-float))
         label500))
      (f2cl-lib:fdo (i 1 (f2cl-lib:int-add i 1))
                    ((> i sbrgns) nil)
        (tagbody
          (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                        ((> j numfun) nil)
            (tagbody
              (setf (f2cl-lib:fref result-%data%
                                   (j)
                                   ((1 numfun))
                                   result-%offset%)
                      (+
                       (f2cl-lib:fref result-%data%
                                      (j)
                                      ((1 numfun))
                                      result-%offset%)
                       (f2cl-lib:fref values$-%data%
                                      (j i)
                                      ((1 numfun) (1 maxsub))
                                      values$-%offset%)))
              (setf (f2cl-lib:fref abserr-%data%
                                   (j)
                                   ((1 numfun))
                                   abserr-%offset%)
                      (+
                       (f2cl-lib:fref abserr-%data%
                                      (j)
                                      ((1 numfun))
                                      abserr-%offset%)
                       (f2cl-lib:fref errors-%data%
                                      (j i)
                                      ((1 numfun) (1 maxsub))
                                      errors-%offset%)))
             label505))
         label510))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref result-%data% (j) ((1 numfun)) result-%offset%)
                  (*
                   (f2cl-lib:fref result-%data%
                                  (j)
                                  ((1 numfun))
                                  result-%offset%)
                   intsgn))
         label600))
      (setf nsub sbrgns)
      (go end_label)
     end_label
      (return
       (values ndim
               numfun
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               neval
               nsub
               ifail
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil
               nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::dadhre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (array double-float (*))
                        (array double-float (*)) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) t (double-float)
                        (double-float) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (array double-float (*)) (array double-float (*))
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)))
           :return-values '(fortran-to-lisp::ndim fortran-to-lisp::numfun nil
                            nil nil nil nil nil nil nil nil nil nil nil nil nil
                            nil fortran-to-lisp::neval fortran-to-lisp::nsub
                            fortran-to-lisp::ifail nil nil nil nil nil nil nil
                            nil nil nil nil nil nil nil nil nil)
           :calls '(fortran-to-lisp::dtrhre fortran-to-lisp::drlhre
                    fortran-to-lisp::dinhre))))

