;;; Compiled by f2cl version:
;;; ("f2cl1.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl2.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl3.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl4.l,v 96616d88fb7e 2008/02/22 22:19:34 rtoy $"
;;;  "f2cl5.l,v 95098eb54f13 2013/04/01 00:45:16 toy $"
;;;  "f2cl6.l,v 1d5cbacbb977 2008/08/24 00:56:27 rtoy $"
;;;  "macros.l,v 1409c1352feb 2013/03/24 20:44:50 toy $")

;;; Using Lisp CMU Common Lisp snapshot-2020-04 (21D Unicode)
;;; 
;;; Options: ((:prune-labels nil) (:auto-save t) (:relaxed-array-decls t)
;;;           (:coerce-assigns :as-needed) (:array-type ':array)
;;;           (:array-slicing t) (:declare-common nil)
;;;           (:float-format single-float))

(in-package "TOMS698")


(defun dtrhre
       (dvflag ndim numfun sbrgns values$ errors centrs hwidts greate error$
        value center hwidth dir)
  (declare (type (array double-float (*)) dir hwidth center value error$ greate
                                          hwidts centrs errors values$)
           (type (f2cl-lib:integer4) sbrgns numfun ndim dvflag))
  (f2cl-lib:with-multi-array-data
      ((values$ double-float values$-%data% values$-%offset%)
       (errors double-float errors-%data% errors-%offset%)
       (centrs double-float centrs-%data% centrs-%offset%)
       (hwidts double-float hwidts-%data% hwidts-%offset%)
       (greate double-float greate-%data% greate-%offset%)
       (error$ double-float error$-%data% error$-%offset%)
       (value double-float value-%data% value-%offset%)
       (center double-float center-%data% center-%offset%)
       (hwidth double-float hwidth-%data% hwidth-%offset%)
       (dir double-float dir-%data% dir-%offset%))
    (prog ((great 0.0d0) (direct 0.0d0) (j 0) (subrgn 0) (subtmp 0))
      (declare (type (f2cl-lib:integer4) subtmp subrgn j)
               (type (double-float) direct great))
      (setf great
              (f2cl-lib:fref greate-%data% (sbrgns) ((1 *)) greate-%offset%))
      (setf direct (f2cl-lib:fref dir-%data% (sbrgns) ((1 *)) dir-%offset%))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j numfun) nil)
        (tagbody
          (setf (f2cl-lib:fref error$-%data% (j) ((1 numfun)) error$-%offset%)
                  (f2cl-lib:fref errors-%data%
                                 (j sbrgns)
                                 ((1 numfun) (1 *))
                                 errors-%offset%))
          (setf (f2cl-lib:fref value-%data% (j) ((1 numfun)) value-%offset%)
                  (f2cl-lib:fref values$-%data%
                                 (j sbrgns)
                                 ((1 numfun) (1 *))
                                 values$-%offset%))
         label5))
      (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                    ((> j ndim) nil)
        (tagbody
          (setf (f2cl-lib:fref center-%data% (j) ((1 ndim)) center-%offset%)
                  (f2cl-lib:fref centrs-%data%
                                 (j sbrgns)
                                 ((1 ndim) (1 *))
                                 centrs-%offset%))
          (setf (f2cl-lib:fref hwidth-%data% (j) ((1 ndim)) hwidth-%offset%)
                  (f2cl-lib:fref hwidts-%data%
                                 (j sbrgns)
                                 ((1 ndim) (1 *))
                                 hwidts-%offset%))
         label10))
      (cond
        ((= dvflag 1)
         (tagbody
           (setf sbrgns (f2cl-lib:int-sub sbrgns 1))
           (setf subrgn 1)
          label20
           (setf subtmp (f2cl-lib:int-mul 2 subrgn))
           (cond
             ((<= subtmp sbrgns)
              (cond
                ((/= subtmp sbrgns)
                 (cond
                   ((< (f2cl-lib:fref greate (subtmp) ((1 *)))
                       (f2cl-lib:fref greate
                                      ((f2cl-lib:int-add subtmp 1))
                                      ((1 *))))
                    (setf subtmp (f2cl-lib:int-add subtmp 1))))))
              (cond
                ((< great (f2cl-lib:fref greate (subtmp) ((1 *))))
                 (setf (f2cl-lib:fref greate-%data%
                                      (subrgn)
                                      ((1 *))
                                      greate-%offset%)
                         (f2cl-lib:fref greate-%data%
                                        (subtmp)
                                        ((1 *))
                                        greate-%offset%))
                 (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                               ((> j numfun) nil)
                   (tagbody
                     (setf (f2cl-lib:fref errors-%data%
                                          (j subrgn)
                                          ((1 numfun) (1 *))
                                          errors-%offset%)
                             (f2cl-lib:fref errors-%data%
                                            (j subtmp)
                                            ((1 numfun) (1 *))
                                            errors-%offset%))
                     (setf (f2cl-lib:fref values$-%data%
                                          (j subrgn)
                                          ((1 numfun) (1 *))
                                          values$-%offset%)
                             (f2cl-lib:fref values$-%data%
                                            (j subtmp)
                                            ((1 numfun) (1 *))
                                            values$-%offset%))
                    label25))
                 (setf (f2cl-lib:fref dir-%data% (subrgn) ((1 *)) dir-%offset%)
                         (f2cl-lib:fref dir-%data%
                                        (subtmp)
                                        ((1 *))
                                        dir-%offset%))
                 (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                               ((> j ndim) nil)
                   (tagbody
                     (setf (f2cl-lib:fref centrs-%data%
                                          (j subrgn)
                                          ((1 ndim) (1 *))
                                          centrs-%offset%)
                             (f2cl-lib:fref centrs-%data%
                                            (j subtmp)
                                            ((1 ndim) (1 *))
                                            centrs-%offset%))
                     (setf (f2cl-lib:fref hwidts-%data%
                                          (j subrgn)
                                          ((1 ndim) (1 *))
                                          hwidts-%offset%)
                             (f2cl-lib:fref hwidts-%data%
                                            (j subtmp)
                                            ((1 ndim) (1 *))
                                            hwidts-%offset%))
                    label30))
                 (setf subrgn subtmp)
                 (go label20)))))))
        ((= dvflag 2)
         (tagbody
           (setf subrgn sbrgns)
          label40
           (setf subtmp (the f2cl-lib:integer4 (truncate subrgn 2)))
           (cond
             ((>= subtmp 1)
              (cond
                ((> great (f2cl-lib:fref greate (subtmp) ((1 *))))
                 (setf (f2cl-lib:fref greate-%data%
                                      (subrgn)
                                      ((1 *))
                                      greate-%offset%)
                         (f2cl-lib:fref greate-%data%
                                        (subtmp)
                                        ((1 *))
                                        greate-%offset%))
                 (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                               ((> j numfun) nil)
                   (tagbody
                     (setf (f2cl-lib:fref errors-%data%
                                          (j subrgn)
                                          ((1 numfun) (1 *))
                                          errors-%offset%)
                             (f2cl-lib:fref errors-%data%
                                            (j subtmp)
                                            ((1 numfun) (1 *))
                                            errors-%offset%))
                     (setf (f2cl-lib:fref values$-%data%
                                          (j subrgn)
                                          ((1 numfun) (1 *))
                                          values$-%offset%)
                             (f2cl-lib:fref values$-%data%
                                            (j subtmp)
                                            ((1 numfun) (1 *))
                                            values$-%offset%))
                    label45))
                 (setf (f2cl-lib:fref dir-%data% (subrgn) ((1 *)) dir-%offset%)
                         (f2cl-lib:fref dir-%data%
                                        (subtmp)
                                        ((1 *))
                                        dir-%offset%))
                 (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                               ((> j ndim) nil)
                   (tagbody
                     (setf (f2cl-lib:fref centrs-%data%
                                          (j subrgn)
                                          ((1 ndim) (1 *))
                                          centrs-%offset%)
                             (f2cl-lib:fref centrs-%data%
                                            (j subtmp)
                                            ((1 ndim) (1 *))
                                            centrs-%offset%))
                     (setf (f2cl-lib:fref hwidts-%data%
                                          (j subrgn)
                                          ((1 ndim) (1 *))
                                          hwidts-%offset%)
                             (f2cl-lib:fref hwidts-%data%
                                            (j subtmp)
                                            ((1 ndim) (1 *))
                                            hwidts-%offset%))
                    label50))
                 (setf subrgn subtmp)
                 (go label40))))))))
      (cond
        ((> sbrgns 0)
         (setf (f2cl-lib:fref greate-%data% (subrgn) ((1 *)) greate-%offset%)
                 great)
         (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                       ((> j numfun) nil)
           (tagbody
             (setf (f2cl-lib:fref errors-%data%
                                  (j subrgn)
                                  ((1 numfun) (1 *))
                                  errors-%offset%)
                     (f2cl-lib:fref error$-%data%
                                    (j)
                                    ((1 numfun))
                                    error$-%offset%))
             (setf (f2cl-lib:fref values$-%data%
                                  (j subrgn)
                                  ((1 numfun) (1 *))
                                  values$-%offset%)
                     (f2cl-lib:fref value-%data%
                                    (j)
                                    ((1 numfun))
                                    value-%offset%))
            label55))
         (setf (f2cl-lib:fref dir-%data% (subrgn) ((1 *)) dir-%offset%) direct)
         (f2cl-lib:fdo (j 1 (f2cl-lib:int-add j 1))
                       ((> j ndim) nil)
           (tagbody
             (setf (f2cl-lib:fref centrs-%data%
                                  (j subrgn)
                                  ((1 ndim) (1 *))
                                  centrs-%offset%)
                     (f2cl-lib:fref center-%data%
                                    (j)
                                    ((1 ndim))
                                    center-%offset%))
             (setf (f2cl-lib:fref hwidts-%data%
                                  (j subrgn)
                                  ((1 ndim) (1 *))
                                  hwidts-%offset%)
                     (f2cl-lib:fref hwidth-%data%
                                    (j)
                                    ((1 ndim))
                                    hwidth-%offset%))
            label60))))
      (go end_label)
     end_label
      (return
       (values nil nil nil sbrgns nil nil nil nil nil nil nil nil nil nil)))))

(in-package #-gcl #:cl-user #+gcl "CL-USER")
#+#.(cl:if (cl:find-package '#:f2cl) '(and) '(or))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf (gethash 'fortran-to-lisp::dtrhre
                 fortran-to-lisp::*f2cl-function-info*)
          (fortran-to-lisp::make-f2cl-finfo
           :arg-types '((fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (fortran-to-lisp::integer4) (fortran-to-lisp::integer4)
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*))
                        (array double-float (*)) (array double-float (*)))
           :return-values '(nil nil nil fortran-to-lisp::sbrgns nil nil nil nil
                            nil nil nil nil nil nil)
           :calls 'nil)))

