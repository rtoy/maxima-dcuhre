# Introduction
This is an interface for Maxima to the dcuhre function, TOMS Algorithm
698: an adaptive multidimensional integration routine for a vector of
integrals.

You must abide by the [ACM Software Copyright Notice](https://www.acm.org/publications/policies/software-copyright-notice)

# How to Use

You can either clone this repo or download the files.  Let's assume
you place the files in `~/.maxima`.  You should have a directory
`maxima-dcuhre` if you cloned the repo or `maxima-dcuhre-master` if
you downloaded the master branch.  For the rest of this we'll assume
all the files are in `~/.maxima/maxima-dcuhre/`.

First start maxima.  To tell maxima how to find these, you need to
execute (or place in your `~/.maxima/maxima-init.mac` start up file):

```
file_search_maxima: append(file_search_maxima, [concat(maxima_userdir, "/maxima-dcuhre/$$$.{mac,mc,wxm}")]);
file_search_lisp: append(file_search_lisp, [concat(maxima_userdir, "/maxima-dcuhre/$$$.{lisp}")]);
```

Test if everything is set up correctly:

```
file_search("dcuhre.mac");
```

This should return the full path to the file `dcuhre.mac`.  If it
doesn't, check the values you used in setting `file_search_maxima` and
`file_search_lisp`.

If `file_search` worked, you can do:

```
load(dcuhre);
```

## Examples
### dtest1
Here's the example from `dtest1.f`:
```
s: z1^2+2*z2^2+3*z3^2+4*z4^2+5*z5^2;
f1: exp(-s/2);
e1: [f1, z1*f1, z2*f1, z3*f1, z4*f1, z5*f1];
toms698_dcuhre(e1, [z1,z2,z3,z4,z5], [0,0,0,0,0], [1,1,1,1,1], key=0, maxpts = 10000, epsabs = 0, epsrel = 1e-3);
```
which produces
```
[0, [0.1385081839650718, 0.06369469129479542, 0.0586174813356963, 
0.05407033699550534, 0.05005614433154001, 0.04654607949240801], 
[9.501503038103273e-8, 1.326013935139371e-7, 8.737276745330629e-6, 
2.07303882665322e-7, 1.916488360463475e-7, 9.090314643633804e-8], 3549]
```
The Fortran output is
```
     FTEST CALLS = 3549, IFAIL =  0
    N   ESTIMATED ERROR   INTEGRAL
    1     0.00000010     0.13850818
    2     0.00000013     0.06369469
    3     0.00000874     0.05861748
    4     0.00000021     0.05407034
    5     0.00000019     0.05005614
    6     0.00000009     0.04654608
```
This matches maxima's output pretty closely.  However, maxima can actually evaluate these integrals analytically:

```
integrate(e1,z5,0,1);
integrate(%,z4,0,1);
integrate(%,z3,0,1);
integrate(%,z2,0,1);
integrate(%,z1,0,1);
```
to get
```
[(%pi^(5/2)*erf(1)*erf(1/sqrt(2))*erf(sqrt(2))*erf(sqrt(3)/sqrt(2))
           *erf(sqrt(5)/sqrt(2)))
  /(16*sqrt(3)*sqrt(5)),
 ((1-1/sqrt(%e))*%pi^2*erf(1)*erf(sqrt(2))*erf(sqrt(3)/sqrt(2))
                *erf(sqrt(5)/sqrt(2)))
  /(2^(7/2)*sqrt(3)*sqrt(5)),
 (%e^-1*(sqrt(2)*%e-sqrt(2))*%pi^2*erf(1/sqrt(2))*erf(sqrt(2))
       *erf(sqrt(3)/sqrt(2))*erf(sqrt(5)/sqrt(2)))
  /(2^(9/2)*sqrt(3)*sqrt(5)),
 (%e^-(3/2)*%pi^2*(%e^(3/2)*erf(1)-erf(1))*erf(1/sqrt(2))*erf(sqrt(2))
           *erf(sqrt(5)/sqrt(2)))
  /(3*2^(7/2)*sqrt(5)),
 (%e^-2*(sqrt(2)*sqrt(3)*%e^2-sqrt(2)*sqrt(3))*%pi^2*erf(1)*erf(1/sqrt(2))
       *erf(sqrt(3)/sqrt(2))*erf(sqrt(5)/sqrt(2)))
  /(96*sqrt(5)),
 (%e^-(5/2)*%pi^2*erf(1)*erf(1/sqrt(2))
           *(sqrt(2)*%e^(5/2)*erf(sqrt(2))-sqrt(2)*erf(sqrt(2)))
           *erf(sqrt(3)/sqrt(2)))
  /(80*sqrt(3))]$
```
Numerical evaluation gives:
```
%,numer;
[0.1385081812348592, 0.06369468099707075, 0.05861746097758764, 
                 0.05407033750324811, 0.05005614699934072, 0.04654607570136497]
```
The absolute error between maxima and the analytical results is:
```
[2.730212611545824e-9, 1.029772467298073e-8, 2.035810865719245e-8, 
         - 5.077427714383909e-10, - 2.667800709510448e-9, 3.791043035050734e-9]
```

### dtest2
Let's try out the example in `dtest2.mac`:

```
eqs: [1,2*z1,3*z1^2,4*z1*z2,4*z1^3,6*z1^2*z2,5*z1^4,8*z1^3*z2,9*z1^2*z2^2,6*z1^5,
 10*z1^4*z2,12*z1^3*z2^2,8*z1^7,10*z1^9,12*z1^11,14*z1^13]$
toms698_dcuhre(eqs,[z1,z2],[0,0],[1,1],key=1, maxpts=195, epsabs=1e-10, epsrel=0.0);
```

which produces 

```
[1, [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 
1.0, 1.0, 1.0], [1.357210303047988e-14, 4.213050999860446e-15, 
1.117299388396991e-14, 7.47420866962098e-15, 2.514418962744505e-15, 
8.71562066877793e-15, 1.703305585767867e-15, 2.432934326301024e-15, 
1.260915907932043e-14, 1.417262999315494e-14, 1.562380940905144e-15, 
8.269643423319683e-15, 1.39148596413286e-14, 5.879022295475152e-6, 
5.383732580075299e-15, 8.522497730107015e-10], 195]
```

Compare this result against the result from Fortran:
```

     DCUHRE TEST WITH NDIM =   2, KEY =  1
     SUBROUTINE CALLS =    195, IFAIL =  1
       N   ABSOLUTE ERROR    INTEGRAL
       1      0.00E+00   1.0000000000000000
       2      0.22E-15   1.0000000000000002
       3      0.22E-15   1.0000000000000002
       4      0.22E-15   1.0000000000000002
       5      0.22E-15   1.0000000000000002
       6      0.22E-15   1.0000000000000002
       7      0.22E-15   1.0000000000000002
       8      0.22E-15   1.0000000000000002
       9      0.44E-15   1.0000000000000004
      10      0.22E-15   1.0000000000000002
      11      0.22E-15   1.0000000000000002
      12      0.22E-15   1.0000000000000002
      13      0.22E-15   1.0000000000000002
      14      0.22E-15   1.0000000000000002
      15      0.44E-15   1.0000000000000004
      16      0.44E-15   1.0000000000000004

```
Just like Fortran, we get IFAIL = 1 with 195 function calls
(NEVAL) and the integrals are all very close to 1, with an ABSERR of about 2e-15.

Note that the functions are just simple polynomials and maxima can evaluate the integrals analytically:
```
integrate(integrate(eqs,z2,0,1),z1,0,1);
[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
```
which matches the computed values as expected.

# Function Documentation

`toms698_dcuhre(fcns, vars, a, b, key = 0, minpts = 0, maxpts = false, epsabs = 1e-10, epsrel=1e-10)`

Computes the following set of integrals
```
  b1  b2     bn
 /   /      /
 [   [      [
 I   I  ... I   [F1, F2, F3, ..., FN] dxn ...  dx2 dx1
 ]   ]      ]
 /   /      /
  a1  a2     an
```
where `n` is the number of variables and `N` is the number of functions with
```
Fm = Fm(x1, x2, ..., xn)
```
for `m = 1, 2, ... N`.  Each function hopefully satisfies the following claim for accuracy:
```
abs(I(k) - result(k)) <= max(epsabs, epsrel * abs(I(k))
```

## Inputs

Let `n` be the length of `vars`, and `N` be the length of `fcns`.  Then

* `fcns` is a list of equations for `F1`, `F2`, ..., `FN`
* `vars` is a list of the independent variables `x1`, `x2`, ..., `xn`
* `a` is a list of the lower limits of integration and must have same length as `vars`
* `b` is the corresponding list of upper limits
* `key` is a keyword argument specifying hte local integration rule
  - 0 the default
    - For `n = 2`, degree 13 rule is used
    - For `n = 3`, degree 11 rule is used
    - For `n > 3`, degree 9 rule is used
  - 1 
    - 2 dimensional degree 13 rule using 65 evaluation points
  - 2
    - 3 dimensional degree 11 rule using 127 evaluation points
  - 3 degree 9 integration rule
  - 4 degree 7 integration rule.  This is recommended for problems that require great adaptivity.
* `minpts` is the minimum number of function evaluations, defaulting to 0
* `maxpts` is the maximum number of funciton evaluations.  If not given `toms698_dcuhre` computes the minimium number allowed for the given key
* `epsabs` requested absolute error, defaulting to 1e-10.
* `epsrel` requested relative error, defaulting to 0

## Output
The output is a list consisting of the following elements

1. ifail indicates the results of the integration
   - 0 for normal exit where `abserr(k) < epsabs` or `abserr(k) <= abs(result(k)*epsrel)` with `maxpts` or less function evaluations
   - 1 if `maxpts` was too small to obtain the required accuracy.
   - 2 `key` is invalid.  Should not happen since the interface checks for valid values.
   - 3 `n` is less than 2 or greater than 15
   - 4 `key = 1` and `n` is not equal to 2
   - 5 `key = 2` and `n` is not equal to 3
   - 6 `N` is less than 1
   - 7 if volume on integration is 0
   - 8 if `maxpts` is too small
   - 9 if `maxpts` is less than minpts. (Should not happen since the interface checks for this.)
   - 10 if `epsabs` < 0 or `epsrel1 < 0
   - 11 if size of work array is too small.  (Should not happen since the interface creates a work array that is large enough
   - 12 if illegal `restar`.  Should not happen because we don't support restart
2. result, a list of length `N` which is an approximation to the value of each integral
3. abserr, a list of length `N` with estimates fo the absolute errors
4. neval, number of function evaluations used

We refer the reader to the documentation in [dcuhre.f](fortran/dcuhre.f) for further details.
